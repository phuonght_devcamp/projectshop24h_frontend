/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function(){
'use strict';

const gBASE_PAYMENT_URL = "http://localhost:8080/payments";


var gListPayment ={ 
  payments:[
 
              {
              }]
             
            }
      
let gPaymentTable = $('#example1').DataTable({
      
        columns: [
    { data: 'id' },
    { data: 'customer.id' },
    { data: 'checkNumber' },
    { data: 'ammount' },
    { data: 'paymentDate' },
    // { data: 'state' },
    // { data: 'country' },
    // { data: 'creditLimit' },
    // { data: 'phoneNumber' },
    // { data: 'postalCode' },
    // { data: 'salesRepEmployeeNumber' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i id="icon-update" class="fas fa-edit text-primary"></i>
      | <i id="icon-delete" class="fas fa-trash text-danger"></i>`,
    },
    
  ],
}
);


var gId="";
const token = getCookie("token");

 function checkTokenRole(){ 
     if(!token) { 
         window.location.href = "cannotaccess.html";
     }
     else {
         $.ajax({
             url: 'http://localhost:8080/hello3',
             type: 'GET',
             beforeSend: function (xhr) {
                 xhr.setRequestHeader('Authorization', 'Token ' +  token);
             },
            
             success: function () { },
             error: function (res) { 
              window.location.href = "cannotaccess.html";
            },
                         })
                     }
     }



 //Hàm get Cookie 
 function getCookie(cname) {
     var name = cname + "=";
     var decodedCookie = decodeURIComponent(document.cookie);
     var ca = decodedCookie.split(';');
     for (var i = 0; i < ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0) == ' ') {
             c = c.substring(1);
         }
         if (c.indexOf(name) == 0) {
             return c.substring(name.length, c.length);
         }
     }
     return "";
 }


//C: Create 
$("#btn-add-new").on("click", function(){
  onBtnCreatePaymentClick();
})
$(".content").on("click", "#btn-confirm-create", function() {
  onBtnConfirmCreatepaymentClick(this);
});
//U: Update  
$("#example1").on("click", "#icon-update", function() {
  onIconEditpaymentClick(this);
});
$("#payment-edit-modal").on("click", "#btn-confirm-edit", function() {
  onBtnConfirmUpdateClick(this);
});

//D: Delete order
$("#example1").on("click", "#icon-delete", function() {
  onIconDeletepaymentClick(this);
});
$('#btn-confirm-delete-payment').on('click', onBtnConfirmDeletepaymentClick);

// $("#modal-delete-all-confirm").on("click", "#btn-confirm-delete-all", function() {
//   onBtnConfirmDeleteAllClick(this);
// });
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
onPageLoading();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm load trang
function onPageLoading(){
  "use strict";
  checkTokenRole();
  callApiGetpaymentData();
}
// hàm xử lý sự kiện goi api delete order
function callApiDeletepayment() {
  "use strict";
  $.ajax({
    url: gBASE_PAYMENT_URL + '/' +  gId ,
    type: "DELETE",
    dataType: 'json',
    success: function(){
      callApiGetpaymentData();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
 }
//Xoá order từ gID của payment
function deletepayment(){
  "use strict";
  callApiDeletepayment();
}
 /**
   * Hàm xử lý khi click confirm
   * Input: paramIconDelete icon click
   * Output: xóa được order và load table
   */
  function onBtnConfirmDeletepaymentClick() {
    "use strict"
    deletepayment();
   // hiển thị lại giao diện
   alert('Delete payment thành công !');
   
   $('#modal-delete-confirm').modal('hide');
 }
// hàm xử lý sự kiện click icon delete
function onIconDeletepaymentClick(paramDeleteIcon) { 
  "use strict"
 
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramDeleteIcon);
  gId = vDataRow.id;
 
  $('#modal-delete-confirm').modal('show');
}
//
function onBtnCreatePaymentClick(){
  "use strict"
  $("#payment-create-modal").modal("show");


  
};



   // hàm xử lý sự kiện goi api update payment
   function callApiUpdatepayment(gpayment) {
    "use strict";
    console.log(gpayment);
    $.ajax({
      url: gBASE_PAYMENT_URL + '/' + gId,
      type: "PUT",
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      data: JSON.stringify(gpayment),
      success: function(responseObject){
       console.log('success update')
        $("#payment-edit-modal").modal("hide");
      callApiGetpaymentData();
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
   }
// hàm validate dữ liệu các trường input
function validateDatapaymentUpdate(){ 
  "use strict";
  if(gpayment.checkNumber =="" )  {
    alert("Vui lòng nhập checkNumber"); 
    return false;
  }
  if(gpayment.ammount == "")  {
    alert("Vui lòng nhập ammount"); 
    return false;
  }
  if(isNaN(gpayment.paymentDate.getTime() ))  {
    alert("Vui lòng chọn PaymentDate"); 
    return false;
  }
  if(gpayment.customer.id === "")  {
    alert("Vui lòng nhập Customer Id"); 
    return false;
  }
  else {return true;}
}
// hàm thu thập dữ liệu payment từ form
function getDatapaymentUpdate(){ 
  "use strict";
  gpayment.checkNumber = $("#md-checknumber-edit").val().trim();
  gpayment.ammount = $("#md-amount-edit").val().trim();
  gpayment.paymentDate = new Date($("#md-paymentDate-edit").val());
  gpayment.customer.id = $("#md-customerId-edit").val().trim();
  
} 
// hàm xử lý sự kiện nút confirm edit payment
function onBtnConfirmUpdateClick() {
  // 1.thu thập dữ liệu payment
  getDatapaymentUpdate();
  //2. validate dữ liệu user
  var vIsDataValidated = validateDatapaymentUpdate();
  if(vIsDataValidated) {
  //3. gọi api update
  callApiUpdatepayment(gpayment);
  
  
  }
}
// hàm hiển thị payment lên modal edit
function loadDataToModalpayment(responseObject) { 
  "use strict"
  //Fill các trường input
  $("#md-checknumber-edit").val(responseObject.checkNumber);
 $("#md-amount-edit").val(responseObject.ammount);
 
var d = Date.parse(responseObject.paymentDate)
var newD = new Date(d).toISOString();

$("#md-paymentDate-edit").val(newD.substring(0,newD.length-1));
 $("#md-customerId-edit").val(responseObject.customer.id);
 
}
 // hàm gọi api get payment by id
 function callApiGetpaymentById() { 
  "use strict"
  $.ajax({
    url: gBASE_PAYMENT_URL + '/'+  gId ,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
      console.log(responseObject)
      loadDataToModalpayment(responseObject);
   },
    error: function(error){
    console.assert(error.responseText);
    }
  });
}
 // hàm thu thập dữ liệu id và orderid
 function getIdFromIcon(paramEditIcon) { 
  "use strict"
  var vTableRow = $(paramEditIcon).parents("tr")
  var vpaymentRowData = gPaymentTable.row(vTableRow).data()
  return vpaymentRowData;
 }
// hàm xử lý sự kiện click icon edit
function onIconEditpaymentClick(paramEditIcon) {
  "use strict"
  // // update trạng thái form
  // gFormMode = gFORM_MODE_UPDATE;
  // $("#div-form-mod").html(gFormMode);
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramEditIcon);
  gId = vDataRow.id;
  
  $("#payment-edit-modal").modal("show");
 
  callApiGetpaymentById();
}
//
function callApiCreatepayment(){ 
  "use strict";
  // Khai báo object gửi lên api create payment
  var vObjectRequest = {
    checkNumber: "",
    ammount: "",
    
    paymentDate: "",
    customer:{id:''},
      };
  
  vObjectRequest = gpayment
    console.log(vObjectRequest);
  $.ajax({
    url: gBASE_PAYMENT_URL ,
    dataType: "json",
    type: "POST",
    contentType: "application/json",
    async: false,  // tham số async nhận giá trị fasle
    data: JSON.stringify(vObjectRequest),
    success: function(responseObject){
      //debugger;
      

      callApiGetpaymentData();
        },
    error: function(error){
      //console.assert(error.responseText);
    }
  })
  //console.log(gReponseObj);
};
// hàm validate dữ liệu các trường input
function validateDatapayment(){ 
  "use strict";
    if(gpayment.checkNumber =="" )  {
      alert("Vui lòng nhập checkNumber"); 
      return false;
    }
    if(gpayment.ammount == "")  {
      alert("Vui lòng nhập ammount"); 
      return false;
    }
    if(isNaN(gpayment.paymentDate.getTime() ))  {
      alert("Vui lòng chọn PaymentDate"); 
      return false;
    }
    if(gpayment.customer.id === "")  {
      alert("Vui lòng nhập Customer Id"); 
      return false;
    }
    else {return true;}
}
// Khai báo đối tượng Person
var gpayment = {
  checkNumber: "",
  ammount: "",
  
  paymentDate: "",
 customer:{id:''}
}
// hàm thu thập dữ liệu payment từ form
function getDatapayment(){
  "use strict";
  gpayment.checkNumber = $("#md-checkNumber").val().trim();
  gpayment.ammount = $("#md-amount").val().trim();
  gpayment.paymentDate = new Date($("#md-paymentDate").val());
  gpayment.customer.id = $("#md-customerId").val().trim();
 
} 
// Hàm bấm nút tạo new payment
function onBtnConfirmCreatepaymentClick(){ 
  "use strict";
  
  // 1.thu thập dữ liệu người order
  getDatapayment();
  //2. validate dữ liệu user
  var vIsDataValidated = validateDatapayment();
  if(vIsDataValidated) {
  //3. gọi api lấy danh sách voucher
  callApiCreatepayment();
  $("#payment-create-modal").modal("hide");
  
  }
}
//Hàm gọi api get danh sách payment
function callApiGetpaymentData(){ 
  "use strict";
  $.ajax({
    url: gBASE_PAYMENT_URL ,
    type: "GET",
    dataType: 'json',
    
  
    success: function(responseObject){
      //debugger;
      gListPayment.payments = responseObject;
    
      loadpaymentToTable(gListPayment.payments); 
      //loadPhongBanToSelect();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}
function loadpaymentToTable(parampayment) {
  gPaymentTable.clear();
  gPaymentTable.rows.add(parampayment);
  gPaymentTable.draw();
}

  
 
// $("#example1").DataTable({ 
//   "responsive": true, "lengthChange": false, "autoWidth": false,"bDestroy":true,
//   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
// }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

})