$(document).ready(function() { 

    const gBASE_URL = "http://localhost:8080"
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
   const token = getCookie("token");
   

    
    //----Phần này làm sau khi đã làm trang info.js

    //Sự kiện bấm nút login
    $("#btn-login").on("click", function() {      
        var username = $("#inputUsername").val();
        var password = $("#inputPassword").val();

        if(validateForm(username, password)) {
            signinForm(username, password);
        }
    });

    function signinForm(username, password) {
        var loginUrl = gBASE_URL + '/login' ;

        var dataObj = {}
       dataObj.username=username;
       dataObj.password=password

        $.ajax({
            type: "POST",
            url: loginUrl,
            data: JSON.stringify(dataObj), 
            dataType:'html',
            contentType: "application/json",
            cache : false,
            
            success: function(responseObject) {
               console.log(responseObject)
               responseHandler(responseObject);
            },
            error: function(error) {
               alert("Wrong username or password")
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {  
        //Lưu token vào cookie trong 1 ngày
        setCookie("token", data, 1);
    window.location.href = "customers.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        console.log(document.cookie)

    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Validate dữ liệu từ form
    function validateForm(username, password) {
        if((username ==="")) {
            alert("Pls fill in username!");
            return false;
        };

        if(password === "") {
            alert("Pls fill in password!");
            return false;
        }

        return true;
    }

    // Hàm validate email bằng regex
    function validateEmail(email) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    }

    //Hàm get Cookie 
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});