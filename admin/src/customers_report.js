/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function(){
'use strict';


const gBASE_CUSTOMER_URL = "http://localhost:8080/customers/";

var gListCustomer ={ 
  customers:[
              {
              }]
              }
      
let gVIPTable = $('#table-member').DataTable({
      
        columns: [
          { data: 'id' },
          { data: 'firstName' },
          { data: 'address' },
          { data: 'phoneNumber' },
          { data: 'totalPayment' },
        ],
        columnDefs: [
          
          {
            targets: 1,
            render: function ( data, type, row, meta ) {
                return row.firstName + ' ' + row.lastName;
            },
          },
          {
            targets: 2,
            width: '200px',
            render: function ( data, type, row, meta ) {
                return row.address + ', ' + row.state + ', ' + row.city + ', '+ row.country;
            },
          },
          
        ],
      }
      );

let gTopBuyerTable = $('#table-sortby-order').DataTable({

  columns: [
    { data: 'id' },
    { data: 'firstName' },
    { data: 'address' },
    { data: 'phoneNumber' },
    { data: 'totalOrder' },
  ],
  columnDefs: [
    
    {
      targets: 1,
      render: function ( data, type, row, meta ) {
          return row.firstName + ' ' + row.lastName;
      },
    },
    {
      targets: 2,
      width: '200px',
      render: function ( data, type, row, meta ) {
          return row.address + ', ' + row.state + ', ' + row.city + ', '+ row.country;
      },
    },
    
  ],
}
);
       
const token = getCookie("token");

 function checkTokenRole(){ 
     if(!token) { 
         window.location.href = "cannotaccess.html";
     }
     else {
         $.ajax({
             url: 'http://localhost:8080/hello3',
             type: 'GET',
             beforeSend: function (xhr) {
                 xhr.setRequestHeader('Authorization', 'Token ' +  token);
             },
            
             success: function () { },
             error: function (res) { 
              window.location.href = "cannotaccess.html";
            },
                         })
                     }
     }



 //Hàm get Cookie 
 function getCookie(cname) {
     var name = cname + "=";
     var decodedCookie = decodeURIComponent(document.cookie);
     var ca = decodedCookie.split(';');
     for (var i = 0; i < ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0) == ' ') {
             c = c.substring(1);
         }
         if (c.indexOf(name) == 0) {
             return c.substring(name.length, c.length);
         }
     }
     return "";
 }


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
//Btn export excel
$("#export-excel").on("click", function() {
  onClickBtnExportExcel();
});

$("#export-excel-order-count").on("click", function() {
  onClickBtnTopOrderExportExcel();
});


//Select member type change
$("#select-member-type").on("change", function() {
  onSelectMemberChange(this);
});

//Select order  type change
$("#select-order-count").on("change", function() {
  onSelectTopOrderChange(this);
});

// $("#modal-delete-all-confirm").on("click", "#btn-confirm-delete-all", function() {
//   onBtnConfirmDeleteAllClick(this);
// });
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
onPageLoading();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm load trang
function onPageLoading(){ 
  "use strict";
  checkTokenRole();
  var vType = $('#select-member-type').val()
  var vTypeOrder = $('#select-order-count').val()
  callApiGetCustomerData(vType);
  callApiGetTopOrdererData(vTypeOrder);
}




//Hàm gọi api get danh sách customer
function callApiGetCustomerData(vType){  
  "use strict";
  $.ajax({
    url: gBASE_CUSTOMER_URL + vType,
    type: "GET",
    dataType: 'json',
    
    success: function(responseObject){ 
      //debugger;
      var vCustomerVIP = getDataCustomerTable(responseObject)
      loadCustomerToTable(vCustomerVIP); 
      console.log(vCustomerVIP)
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}

function callApiGetTopOrdererData(vTypeOrder){  
  "use strict";
  $.ajax({
    url: gBASE_CUSTOMER_URL + vTypeOrder,
    type: "GET",
    dataType: 'json',
    
    success: function(responseObject){ 
      //debugger;
      var vTopOrderer = getDataTopOrderTable(responseObject)
      loadTopOrdererToTable(vTopOrderer); 
      //console.log(vCustomerVIP)
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}


function getDataCustomerTable(responseObject){
  var gCustomerVIP = {}
  
  gCustomerVIP = Array.from({ length: responseObject.length } , () => ({ 
        id: '',
        address: '',
        city:'', 
        country:'',
        firstName:'',
        lastName:'',
        phoneNumber:'',
        state:'',
        totalPayment:'',
        }))

for (var i=0; i<gCustomerVIP.length;i++){
gCustomerVIP[i].id = responseObject[i][0];
gCustomerVIP[i].address = responseObject[i][1];
gCustomerVIP[i].city = responseObject[i][2];
gCustomerVIP[i]. country = responseObject[i][3];
gCustomerVIP[i].firstName = responseObject[i][5];
gCustomerVIP[i].lastName = responseObject[i][6];
gCustomerVIP[i].phoneNumber = responseObject[i][7];
gCustomerVIP[i].state = responseObject[i][10];
gCustomerVIP[i].totalPayment = responseObject[i][12];
} 
return gCustomerVIP;
}

function getDataTopOrderTable(responseObject){
  var vTopOrderer = {}
  
  vTopOrderer = Array.from({ length: responseObject.length } , () => ({ 
        id: '',
        address: '',
        city:'', 
        country:'',
        firstName:'',
        lastName:'',
        phoneNumber:'',
        state:'',
        totalOrder:'',
        }))

for (var i=0; i<vTopOrderer.length;i++){
  vTopOrderer[i].id = responseObject[i][0];
  vTopOrderer[i].address = responseObject[i][1];
  vTopOrderer[i].city = responseObject[i][2];
  vTopOrderer[i]. country = responseObject[i][3];
  vTopOrderer[i].firstName = responseObject[i][5];
  vTopOrderer[i].lastName = responseObject[i][6];
vTopOrderer[i].phoneNumber = responseObject[i][7];
vTopOrderer[i].state = responseObject[i][10];
vTopOrderer[i].totalOrder = responseObject[i][12];
} 
return vTopOrderer;
}




function loadCustomerToTable(paramCustomer) {
  gVIPTable.clear();
  gVIPTable.rows.add(paramCustomer);
  gVIPTable.draw();
}


function loadTopOrdererToTable(paramCustomer) {
  gTopBuyerTable.clear();
  gTopBuyerTable.rows.add(paramCustomer);
  gTopBuyerTable.draw();
}

function onSelectMemberChange(paramSelect){ 

  var vType = $(paramSelect).val();
  callApiGetCustomerData(vType);
}

function onSelectTopOrderChange(paramSelect){ 

  var vType = $(paramSelect).val();
  callApiGetTopOrdererData(vType);
}

function onClickBtnExportExcel(){ 
  var vType= $('#select-member-type').val();
  window.location.href = "http://localhost:8080/export/customers/" + vType+"/excel"
}

function onClickBtnTopOrderExportExcel(){  debugger
  var vType= $('#select-order-count').val();
  window.location.href = "http://localhost:8080/export/customers/" + vType+"/excel"
}
 



})