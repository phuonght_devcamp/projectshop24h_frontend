/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function(){
'use strict';


const gBASE_ORDER_URL = "http://localhost:8080/orders";
const gBASE_ORDER_DETAIL_URL =  "http://localhost:8080/order-details"

var gListOrders ={ 
  orders:[
 
              {
              }]
              };

var gOrderObj={};
 
              
      
let gOrderTable = $('#example1').DataTable({
  "order": [[ 1, "desc" ]] ,
      columns: [
        { data: 'id' },
        { data: 'orderDate' },
        { data: 'requiredDate' },
        { data: 'shippedDate' },
        { data: 'status' },
        { data: 'comments' },
        { data: 'action' }
      ],
      columnDefs: [
        {
          targets: -1,
          defaultContent: `<i id="icon-view" title="View Order Detail" class="fas fa-eye text-warning"></i> |
          <i id="icon-update" title="Update Info" class="fas fa-edit text-primary"></i>
          | <i id="icon-delete" title="Delete" class="fas fa-trash text-danger"></i>`,
        },
        {
          targets: [1,2,3],
          render: function ( data, type, row ) {
            if (data !== null){
              return data.substring(0,10);
            }
            else return data;
           }
        }]
      });

    


var gOrderTableMonth = $('#table-order-month').DataTable({
    
      columns: [
        { data: 'id' },
        { data: 'orderDate' },
        { data: 'requiredDate' },
        { data: 'shippedDate' },
        { data: 'status' },
        { data: 'comments' }
       
      ],
      columnDefs: [
        
        {
          targets: [1,2,3],
          render: function ( data, type, row ) {
            if (data !== null){
              return data.substring(0,10);
            }
            else return data;
           }
        }]
      });

var gOrderTableWeek = $('#table-order-week').DataTable({

  columns: [
    { data: 'id' },
    { data: 'orderDate' },
    { data: 'requiredDate' },
    { data: 'shippedDate' },
    { data: 'status' },
    { data: 'comments' }
    
  ],
  columnDefs: [
    
    {
      targets: [1,2,3],
      render: function ( data, type, row ) {
        if (data !== null){
          return data.substring(0,10);
        }
        else return data;
        }
    }]
  });

var gOrderDetailTable = $('#order-detail-table').DataTable({
  columns: [
    { data: 'id' },
    
    { data: 'quantityOrder' },
    { data: 'priceEach' },
    { data: 'subTotal' },
    { data: 'action' }
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `
       <i id="view-icon-delete" title="Delete" class="fas fa-trash text-danger"></i>`,
    },
    {
      targets: 3,
      render: function(data,type,row){ 
        return row.priceEach*row.quantityOrder;
      }
    }
  ]
});

const token = getCookie("token");

 function checkTokenRole(){ 
     if(!token) { 
         window.location.href = "cannotaccess.html";
     }
     else {
         $.ajax({
             url: 'http://localhost:8080/hello3',
             type: 'GET',
             beforeSend: function (xhr) {
                 xhr.setRequestHeader('Authorization', 'Token ' +  token);
             },
            
             success: function () { },
             error: function (res) { 
              window.location.href = "cannotaccess.html";
            },
                         })
                     }
     }



 //Hàm get Cookie 
 function getCookie(cname) {
     var name = cname + "=";
     var decodedCookie = decodeURIComponent(document.cookie);
     var ca = decodedCookie.split(';');
     for (var i = 0; i < ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0) == ' ') {
             c = c.substring(1);
         }
         if (c.indexOf(name) == 0) {
             return c.substring(name.length, c.length);
         }
     }
     return "";
 }


var gId="";
// var gRowData = {};


//C: Create 
$("#btn-add-new").on("click", function(){
  onBtnCreateOrderClick();
})
$(".content").on("click", "#btn-confirm-create", function() {
  onBtnConfirmCreateOrderClick(this);
});
//U: Update  
$("#example1").on("click", "#icon-update", function() {
  onIconEditOrderClick(this);
});
$("#order-edit-modal").on("click", "#btn-confirm-edit", function() {
  onBtnConfirmUpdateClick(this);
});

//D: Delete order
$("#example1").on("click", "#icon-delete", function() {
  onIconDeleteOrderClick(this);
});

$('#btn-confirm-delete-order').on("click", function(){ onBtnConfirmDeleteOrderClick()});

$('#export-excel').on("click", function(){ fnExcelReport()});

$('#export-excel-month').on("click", function(){ fnExcelReportMonth()});

$('#export-excel-week').on("click", function(){ fnExcelReportWeek()});
//delete order detail
$("#orderDetail-modal").on("click", "#view-icon-delete", function() {
  onIconDeleteOrderDetail(this);
});
// $("#modal-delete-all-confirm").on("click", "#btn-confirm-delete-all", function() {
//   onBtnConfirmDeleteAllClick(this);
// });


$('#btn-confirm-order-receive').on('click', function(){
  $('#modal-order-receive').modal('hide');
 window.location.href='orders.html'
})
// Icon view orderdetail
$("#example1").on("click", "#icon-view", function() {
  onIconViewOrderDetailClick(this);
});

var gDataOrderDetailTable ={}
function onIconViewOrderDetailClick(paramViewIcon){ 
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramViewIcon);
  gId = vDataRow.id;
  $('#orderDetail-modal').modal('show');
  callApiGetOrderDetailsByOrderId();
}

function loadOrderDetailToTable(responseObject){ 
  gOrderDetailTable.clear();
  gOrderDetailTable.rows.add(responseObject);
  gOrderDetailTable.draw();
}

function callApiGetOrderDetailsByOrderId(){
  "use strict"
  $.ajax({
    url: gBASE_ORDER_DETAIL_URL +'/orders/'+ gId ,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){ 
      //console.log(responseObject)
      loadOrderDetailToTable(responseObject);
   },
    error: function(error){
    console.assert(error.responseText);
    }
  });
}


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
onPageLoading();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm load trang
function onPageLoading(){ 
  "use strict";
  checkTokenRole();
  callApiGetOrderData();
  setInterval(function () { 
    callApiGetOrderData();
    var tableLengthBefore  =   gOrderTable.data().count();
    $('#example1').on( 'draw.dt', function (  ) {
      var tableLengthAfter  =   gOrderTable.data().count();
      if(tableLengthAfter > tableLengthBefore ){

        var audio = new Audio('src/audio/Message Received.mp3');
        audio.play();
       $('#modal-order-receive').modal('show')
      }
  } );
}, 10000);

  callApiGetOrderDataMonth();
  callApiGetOrderDataWeek();
}
// hàm xử lý sự kiện goi api delete order
function callApiDeleteOrder() {
  "use strict";
  $.ajax({
    url: gBASE_ORDER_URL +'/'+ gId ,
    type: "DELETE",
    dataType: 'json',
    success: function(responseObject){
      alert('Delete order Success !');
      callApiGetOrderData();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
 }
 /**
   * Hàm xử lý khi click confirm
   * Input: paramIconDelete icon click
   * Output: xóa được order và load table
   */
  function onBtnConfirmDeleteOrderClick() { 
    "use strict"
    console.log('sss');
   callApiDeleteOrder();
   // hiển thị lại giao diện
   $('#modal-delete-confirm').modal('hide');
 }
// hàm xử lý sự kiện click icon delete
function onIconDeleteOrderClick(paramDeleteIcon) {  
  "use strict"
 
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramDeleteIcon);
  gId = vDataRow.id;
 
  $('#modal-delete-confirm').modal('show');
}

// click create
function onBtnCreateOrderClick(){
  "use strict"
  $("#order-create-modal").modal("show");
};



   // hàm xử lý sự kiện goi api update order
   function callApiUpdateOrder(gOrderObj) { 
    "use strict";
    $.ajax({
      url: gBASE_ORDER_URL +'/'+ gId,
      type: "PUT",
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      data: JSON.stringify(gOrderObj),
      success: function(responseObject){
        $("#order-edit-modal").modal("hide");
        callApiGetOrderData();
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
   }
// hàm validate dữ liệu các trường input
// function validateDataOrder(){ 
//   "use strict";
//     if(gOrderObj.orderDate =="" )  {
//       alert("Vui lòng nhập orderDate"); 
//       return false;
//     }
//     if(gOrderObj.requiredDate == "")  {
//       alert("Vui lòng nhập requiredDate"); 
//       return false;
//     }
//     if(gOrderObj.shippedDate === "")  {
//       alert("Vui lòng nhập shippedDate"); 
//       return false;
//     }
//     else {return true;}
// }
// hàm thu thập dữ liệu order từ form
function getDataOrderUpdate(){
  "use strict";
  gOrderObj.orderDate = moment($("#md-orderDate-edit").val()).toISOString();
  gOrderObj.requiredDate =moment( $("#md-requiredDate-edit").val()).toISOString();
  gOrderObj.shippedDate = moment($("#md-shippedDate-edit").val()).toISOString();
  gOrderObj.status = $("#md-status-edit").val().trim();
  gOrderObj.comments = $("#md-comments-edit").val().trim();
} 
// hàm xử lý sự kiện nút confirm edit order
function onBtnConfirmUpdateClick() {
  // 1.thu thập dữ liệu order
  getDataOrderUpdate();
  //2. validate dữ liệu user
  // var vIsDataValidated = validateDataOrder();
  // if(vIsDataValidated) {
  //3. gọi api update
// }
  callApiUpdateOrder(gOrderObj);
  $("#order-edit-modal").modal("hide");
  
}
// hàm hiển thị order lên modal edit
function loadDataToModalOrder(responseObject) { 
  "use strict"
  //Fill các trường input
  var dateOrder = moment(responseObject.orderDate).utc().format('YYYY-MM-DD');
  var dateRequired = moment(responseObject.requiredDate).utc().format('YYYY-MM-DD');
  var dateShipped = moment(responseObject.shippedDate).utc().format('YYYY-MM-DD');

  $("#md-orderDate-edit").val(dateOrder);
 $("#md-requiredDate-edit").val(dateRequired);
 $("#md-shippedDate-edit").val(dateShipped)
$("#md-status-edit ").val(responseObject.status).change();
 $("#md-comments-edit").val(responseObject.comments);
}
 // hàm gọi api get order by id
 function callApiGetOrderById() { 
  "use strict"
  $.ajax({
    url: gBASE_ORDER_URL +'/'+ gId ,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
    
      loadDataToModalOrder(responseObject);
   },
    error: function(error){
    console.assert(error.responseText);
    }
  });
}
 // hàm thu thập dữ liệu id và orderid
 function getIdFromIcon(paramEditIcon) { 
  "use strict"
  var vTableRow = $(paramEditIcon).parents("tr")
  var vRowData = gOrderTable.row(vTableRow).data()
  return vRowData;
 }
// hàm xử lý sự kiện click icon edit
function onIconEditOrderClick(paramEditIcon) {
  "use strict"
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramEditIcon);
  gId = vDataRow.id;
  
  $("#order-edit-modal").modal("show");
 
  callApiGetOrderById();
}

var gCustomerId='';
function callApiCreateOrder(){  
  "use strict";
  // Khai báo object gửi lên api create order
  var vObjectRequest = {
    orderDate: "",
    requiredDate: "",
    shippedDate: "",
    status: "",
    comments: '',
      };
  
  vObjectRequest = gOrderObj;
 
  $.ajax({
    url: gBASE_ORDER_URL+"/"+ gCustomerId,
    dataType: "json",
    type: "POST",
    contentType: "application/json",
    async: false,  // tham số async nhận giá trị fasle
    data: JSON.stringify(vObjectRequest),
    success: function(responseObject){
      callApiGetOrderData();
        alert("Create Order Success!");
        },
    error: function(error){
      //console.assert(error.responseText);
    }
  })
  //console.log(gReponseObj);
};
// hàm validate dữ liệu các trường input
function validateDataOrder(){ 
  "use strict";
    if(gOrderObj.orderDate =="" )  {
      alert("Vui lòng nhập orderDate"); 
      return false;
    }
    if(gOrderObj.requiredDate == "")  {
      alert("Vui lòng nhập requiredDate"); 
      return false;
    }
    if(gOrderObj.shippedDate === "")  {
      alert("Vui lòng nhập shippedDate"); 
      return false;
    }
    else {return true;}
}
// Khai báo đối tượng Order
var gOrderObj = {
  orderDate: "",
  requiredDate: "",
  shippedDate: "",
  status: "",
  comments: '',
}
// hàm thu thập dữ liệu order từ form
function getDataOrder(){
  "use strict";
  gOrderObj.orderDate = moment($("#md-orderDate").val()).toISOString();
  gOrderObj.requiredDate =moment( $("#md-requiredDate").val()).toISOString();
  gOrderObj.shippedDate = moment($("#md-shippedDate").val()).toISOString();
  gOrderObj.status = $("#md-status").val().trim();
  gOrderObj.comments = $("#md-comments").val().trim();
  gCustomerId = $('#md-customerId').val().trim();
} 
// Hàm bấm nút tạo new order
function onBtnConfirmCreateOrderClick(){ 
  "use strict";
  // 1.thu thập dữ liệu  order
  getDataOrder();
  // //2. validate dữ liệu 
  // var vIsDataValidated = validateDataOrder();
  // if(vIsDataValidated) {
  //3. gọi api lấy danh sách order
  callApiCreateOrder();
  $("#order-create-modal").modal("hide");
}
//Hàm gọi api get danh sách order
function callApiGetOrderData(){ 
  "use strict";
  $.ajax({
    url: gBASE_ORDER_URL ,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
      //debugger;
      gListOrders.orders = responseObject;
      console.log(gListOrders)
      loadOrderToTable(gListOrders.orders); 
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}

//load dữ liệu vào bảng
function loadOrderToTable(paramCustomer) {
  gOrderTable.clear();
  gOrderTable.rows.add(paramCustomer);
  gOrderTable.draw();
}




function fnExcelReport() { 
  var table = document.getElementById('example1'); // id of table
  var tableHTML = table.outerHTML;
  var fileName = 'orders.xls';
  
  var msie = window.navigator.userAgent.indexOf("MSIE ");
  
  // If Internet Explorer
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    dummyFrame.document.open('txt/html', 'replace');
    dummyFrame.document.write(tableHTML);
    dummyFrame.document.close();
    dummyFrame.focus();
    return dummyFrame.document.execCommand('SaveAs', true, fileName);
  }
  //other browsers
  else {
    var a = document.createElement('a');
    tableHTML = tableHTML.replace(/  /g, '').replace(/ /g, '%20'); // replaces spaces
    a.href = 'data:application/vnd.ms-excel,' + tableHTML;
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

function fnExcelReportMonth() { 
  var table = document.getElementById('table-order-month'); // id of table
  var tableHTML = table.outerHTML;
  var fileName = 'ordersLast30day.xls';
  
  var msie = window.navigator.userAgent.indexOf("MSIE ");
  
  // If Internet Explorer
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    dummyFrame.document.open('txt/html', 'replace');
    dummyFrame.document.write(tableHTML);
    dummyFrame.document.close();
    dummyFrame.focus();
    return dummyFrame.document.execCommand('SaveAs', true, fileName);
  }
  //other browsers
  else {
    var a = document.createElement('a');
    tableHTML = tableHTML.replace(/  /g, '').replace(/ /g, '%20'); // replaces spaces
    a.href = 'data:application/vnd.ms-excel,' + tableHTML;
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

var gListOrdersMonth = {}
//Hàm gọi api get danh sách order thangs
function callApiGetOrderDataMonth(){ 
  "use strict";
  $.ajax({
    url: gBASE_ORDER_URL  +'/month',
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
      //debugger;
      gListOrdersMonth.orders = responseObject;
      //console.log(gListOrders)
      loadOrderToTableMonth(gListOrdersMonth.orders); 
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}

//load dữ liệu vào bảng
function loadOrderToTableMonth(gListOrdersMonth) {
  gOrderTableMonth.clear();
  gOrderTableMonth.rows.add(gListOrdersMonth);
  gOrderTableMonth.draw();
}


function fnExcelReportWeek() { 
  var table = document.getElementById('table-order-week'); // id of table
  var tableHTML = table.outerHTML;
  var fileName = 'ordersLast7day.xls';
  
  var msie = window.navigator.userAgent.indexOf("MSIE ");
  
  // If Internet Explorer
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    dummyFrame.document.open('txt/html', 'replace');
    dummyFrame.document.write(tableHTML);
    dummyFrame.document.close();
    dummyFrame.focus();
    return dummyFrame.document.execCommand('SaveAs', true, fileName);
  }
  //other browsers
  else {
    var a = document.createElement('a');
    tableHTML = tableHTML.replace(/  /g, '').replace(/ /g, '%20'); // replaces spaces
    a.href = 'data:application/vnd.ms-excel,' + tableHTML;
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

var gListOrdersWeek = {}
//Hàm gọi api get danh sách order thangs
function callApiGetOrderDataWeek(){ 
  "use strict";
  $.ajax({
    url: gBASE_ORDER_URL  +'/week',
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
      //debugger;
      gListOrdersWeek.orders = responseObject;
      //console.log(gListOrders)
      loadOrderToTableWeek(gListOrdersWeek.orders); 
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}

//load dữ liệu vào bảng
function loadOrderToTableWeek(gListOrdersWeek) {
  gOrderTableWeek.clear();
  gOrderTableWeek.rows.add(gListOrdersWeek);
  gOrderTableWeek.draw();
}

var gOrderDetailId = '';

// hàm thu thập dữ liệu id và order detail id
function getIdFromIconDeleteOrderDetail(paramDeleteIcon) { 
  "use strict"
  var vTableRow = $(paramDeleteIcon).parents("tr")
  var vRowData = gOrderDetailTable.row(vTableRow).data()
  return vRowData;
}
function onIconDeleteOrderDetail(paramDeleteIcon){
  var vDataRow = getIdFromIconDeleteOrderDetail(paramDeleteIcon);
  gOrderDetailId = vDataRow.id;
  callApiDeleteOrderDetailById();
}

function callApiDeleteOrderDetailById(){
  "use strict";
  $.ajax({
    url: gBASE_ORDER_DETAIL_URL +'/'+ gOrderDetailId ,
    type: "DELETE",
    dataType: 'json',
    success: function(responseObject){
      alert('Delete order detail Success !');
      callApiGetOrderDetailsByOrderId(gOrderDetailId);
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}


})