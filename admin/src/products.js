/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function(){
'use strict';

const gBASE_PRODUCT_URL = "http://localhost:8080/products";

var gListProduct ={ 
  products:[
 
              {
              }]
             
              }
            
      
let gProductTable = $('#example1').DataTable({
        //bDestroy: true,
    autoWidth:false,
    columns: [
    { data: 'id' },
    { data: 'productCode' },
    { data: 'productName' },
    { data: 'buyPrice' },
    { data: 'imgURL' },
    { data: 'quantityInStock' },
    { data: 'productDescription' },
    { data: 'productScale' },
    { data: 'productVendor' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i id="icon-update" class="fas fa-edit text-primary"></i>
      | <i id="icon-delete" class="fas fa-trash text-danger"></i>`,
    },
    {targets: 0, width:'20px'},
    {targets: 1, width:'60px'},
    {targets: 2, width:'60px'},
    {targets: 3, width:'40px'},
    {targets: 4, 
      width:'100px',
      render: function(data,row){ 
        console.log(data)
        

        var vImg =  `<img src="../client/`+ data+`"  height="100" width="100" />` 
        return vImg;
      }
    },
    {targets: 5, width:'40px'},
    {targets: 6, width:'120px'}
  ],
}
);

const token = getCookie("token");

 function checkTokenRole(){ 
     if(!token) { 
         window.location.href = "cannotaccess.html";
     }
     else {
         $.ajax({
             url: 'http://localhost:8080/hello3',
             type: 'GET',
             beforeSend: function (xhr) {
                 xhr.setRequestHeader('Authorization', 'Token ' +  token);
             },
            
             success: function () { },
             error: function (res) { 
              window.location.href = "cannotaccess.html";
            },
                         })
                     }
     }



 //Hàm get Cookie 
 function getCookie(cname) {
     var name = cname + "=";
     var decodedCookie = decodeURIComponent(document.cookie);
     var ca = decodedCookie.split(';');
     for (var i = 0; i < ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0) == ' ') {
             c = c.substring(1);
         }
         if (c.indexOf(name) == 0) {
             return c.substring(name.length, c.length);
         }
     }
     return "";
 }


var gId="";
var gRowData = {};

//C: Create 
$("#btn-add-new").on("click", function(){
  onBtnCreateProductClick();
})
$(".content").on("click", "#btn-confirm-create", function() {
  onBtnConfirmCreateProductClick(this);
});
//U: Update  
$("#example1").on("click", "#icon-update", function() {
  onIconEditProductClick(this);
});
$("#Product-edit-modal").on("click", "#btn-confirm-edit", function() {
  onBtnConfirmUpdateClick(this);
});

//D: Delete order
$("#example1").on("click", "#icon-delete", function() {
  onIconDeleteProductClick(this);
});
$('#btn-confirm-delete-Product').on('click', onBtnConfirmDeleteProductClick);

//browse, upload img
$('#Product-create-modal').on("click", ".browse", function() {
  var file = $(this)
    .parent()
    .parent()
    .parent()
    .find(".file");
  file.trigger("click");
});



$('input[type="file"]').change(function(e) {
  var fileName = e.target.files[0].name;
  $("#file").val(fileName);
 
  var reader = new FileReader();
  reader.onload = function(e) {
    // get loaded data and render thumbnail.
    document.getElementById("preview").src = e.target.result;
  };
  // read the image file as a data URL.
  reader.readAsDataURL(this.files[0]);
});
$("#submit").on("click", function(e) {  

  e.preventDefault();
  var formData = new FormData();
  formData.append('file', $('input[type=file]')[0].files[0])
  $("#msg").html('<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> Please wait...!</div>');
  debugger
  $.ajax({
    type: "POST",
    url: "http://localhost:8080/uploadFile",
    data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false, // The content type used when sending data to the server.
    cache: false, // To unable request pages to be cached
    processData: false, // To send DOMDocument or non processed data file it is set to false
    success: function(res) {
    console.log(res)
    $("#msg").html(
      '<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Image upload successfully.</div>'
    );
    $("#md-imgURL").val('img/'  + res.fileName)
  },
    error: function(data) {
      $("#msg").html(
        '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong.</div>'
      );
    }
  });

});

//browse, upload img in edit modal
$('#Product-edit-modal').on("click", ".browse-edit", function() {
  var file = $(this)
    .parent()
    .parent()
    .parent()
    .find(".file-edit");
  file.trigger("click");
});



$('.file-edit ').change(function(e) {
  var fileName = e.target.files[0].name;
  $("#file-edit").val(fileName);
 
  var readerEdit = new FileReader();
  readerEdit.onload = function(e) {
    // get loaded data and render thumbnail.
    document.getElementById("preview-edit").src = e.target.result;
  };
  // read the image file as a data URL.
  readerEdit.readAsDataURL(this.files[0]);
});
$("#submit-edit").on("click", function(e) {   debugger

  e.preventDefault();
  var formDataEdit = new FormData();
  formDataEdit.append('file', $('.file-edit')[0].files[0])
  $("#msg-edit").html('<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> Please wait...!</div>');
  
  $.ajax({
    type: "POST",
    url: "http://localhost:8080/uploadFile",
    data: formDataEdit, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false, // The content type used when sending data to the server.
    cache: false, // To unable request pages to be cached
    processData: false, // To send DOMDocument or non processed data file it is set to false
    success: function(res) {
    console.log(res)
    $("#msg-edit").html(
      '<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Image upload successfully.</div>'
    );
    $("#md-imgURL-edit").val('img/'  + res.fileName)
  },
    error: function(data) {
      $("#msg-edit").html(
        '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong.</div>'
      );
    }
  });

});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
onPageLoading();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm load trang
function onPageLoading(){
  "use strict";
  checkTokenRole();
  callApiGetProductData();
}
// hàm xử lý sự kiện goi api delete order
function callApiDeleteProduct() {
  "use strict";
  $.ajax({
    url: gBASE_PRODUCT_URL +'/'+ gId ,
    type: "DELETE",
    dataType: 'json',
    success: function(){
      callApiGetProductData();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
 }
//Xoá order từ gID của Product
function deleteProduct(){
  "use strict";
  callApiDeleteProduct();
}
 /**
   * Hàm xử lý khi click confirm
   * Input: paramIconDelete icon click
   * Output: xóa được order và load table
   */
  function onBtnConfirmDeleteProductClick() {
    "use strict"
    deleteProduct();
   // hiển thị lại giao diện
   alert('Delete product thành công !');
   
   $('#modal-delete-confirm').modal('hide');
 }
// hàm xử lý sự kiện click icon delete
function onIconDeleteProductClick(paramDeleteIcon) { 
  "use strict"
 
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramDeleteIcon);
  gId = vDataRow.id;
 
  $('#modal-delete-confirm').modal('show');
}
//
function onBtnCreateProductClick(){
  "use strict"
  $("#Product-create-modal").modal("show");
};

   // hàm xử lý sự kiện goi api update Product
   function callApiUpdateProduct(gProduct) { 
    "use strict";
    $.ajax({
      url: gBASE_PRODUCT_URL + '/'+gId,
      type: "PUT",
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      data: JSON.stringify(gProduct),
      success: function(responseObject){
       
        $("#Product-edit-modal").modal("hide");
        callApiGetProductData();
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
   }
// hàm validate dữ liệu các trường input
function validateDataProductUpdate(){ 
  "use strict";
    if(gProduct.productCode =="" )  {
      alert("Vui lòng nhập productCode"); 
      return false;
    }
    if(gProduct.productName == "")  {
      alert("Vui lòng nhập productName"); 
      return false;
    }
    if(gProduct.productDescription === "")  {
      alert("Vui lòng nhập productDescription"); 
      return false;
    }
    else {return true;}
}
// hàm thu thập dữ liệu Product từ form
function getDataProductUpdate(){
  "use strict";
  gProduct.productCode = $("#md-productCode-edit").val().trim();
  gProduct.productName = $("#md-productName-edit").val().trim();
  gProduct.buyPrice = $("#md-buyPrice-edit").val().trim();
  gProduct.imgURL =  $("#md-imgURL-edit").val().trim(); 
  gProduct.productDescription = $("#md-productDescription-edit").val()
  gProduct.productScale = $("#md-productScale-edit").val()
  gProduct.quantityInStock = $("#md-quantityInStock-edit").val()
  gProduct.productVendor = $("#md-productVendor-edit").val()
} 
// hàm xử lý sự kiện nút confirm edit Product
function onBtnConfirmUpdateClick() {
  // 1.thu thập dữ liệu Product
  getDataProductUpdate();
  //2. validate dữ liệu user
  var vIsDataValidated = validateDataProductUpdate();
  if(vIsDataValidated) {
  //3. gọi api update
  callApiUpdateProduct(gProduct);
  $("#Product-edit-modal").modal("hide");
  
  }
}
// hàm hiển thị Product lên modal edit
function loadDataToModalProduct(responseObject) { ;
  "use strict"
  var dateCreate = moment(responseObject.createdAt).utc().format('YYYY-MM-DD');
  //Fill các trường input
  $('.title-product').html(responseObject.productName)
  $("#md-createdAt-edit").val(dateCreate);
  $("#md-productCode-edit").val(responseObject.productCode);
 $("#md-productName-edit").val(responseObject.productName);
$("#md-buyPrice-edit").val(responseObject.buyPrice);
 $("#md-imgURL-edit").val(responseObject.imgURL);
$("#md-productDescription-edit").val(responseObject.productDescription)
$("#md-productScale-edit").val(responseObject.productScale)
$("#md-quantityInStock-edit").val(responseObject.quantityInStock)
$("#md-productVendor-edit").val(responseObject.productVendor)
}
 // hàm gọi api get Product by id
 function callApiGetProductById() { 
  "use strict"
  $.ajax({
    url: gBASE_PRODUCT_URL +'/'+ gId ,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
    
      loadDataToModalProduct(responseObject);
   },
    error: function(error){
    console.assert(error.responseText);
    }
  });
}
 // hàm thu thập dữ liệu id và orderid
 function getIdFromIcon(paramEditIcon) { 
  "use strict"
  var vTableRow = $(paramEditIcon).parents("tr")
  var vProductRowData = gProductTable.row(vTableRow).data()
  return vProductRowData;
 }
// hàm xử lý sự kiện click icon edit
function onIconEditProductClick(paramEditIcon) {
  "use strict"
  // // update trạng thái form
  // gFormMode = gFORM_MODE_UPDATE;
  // $("#div-form-mod").html(gFormMode);
  //lưu thông tin id, orderid vào biến toàn cục
  var vDataRow = getIdFromIcon(paramEditIcon);
  gId = vDataRow.id;
  
  $("#Product-edit-modal").modal("show");
 
  callApiGetProductById();
}
//
function callApiCreateProduct(){ 
  "use strict";
  // Khai báo object gửi lên api create Product
  var vObjectRequest = {
    productCode: "",
    productName: "",
    productDescription: "",
    productScale: "",
    productVendor: 0,
    quantityInStock: 0,
    buyPrice: "",
    imgURL: "",
      };
  
  vObjectRequest = gProduct
    debugger
  $.ajax({
    url: gBASE_PRODUCT_URL,
    dataType: "json",
    type: "POST",
    contentType: "application/json",
    async: false,  // tham số async nhận giá trị fasle
    data: JSON.stringify(vObjectRequest),
    success: function(responseObject){
      callApiGetProductData();
        },
    error: function(error){
      console.assert(error.responseText);
    }
  })
  //console.log(gReponseObj);
};
// hàm validate dữ liệu các trường input
function validateDataProduct(){ 
  "use strict";
    if(gProduct.productCode =="" )  {
      alert("Vui lòng nhập productCode"); 
      return false;
    }
    if(gProduct.productName == "")  {
      alert("Vui lòng nhập productName"); 
      return false;
    }
    if(gProduct.productDescription === "")  {
      alert("Vui lòng nhập productDescription"); 
      return false;
    }
    else {return true;}
}
// Khai báo đối tượng product
var gProduct = {
  productCode: "",
  productName: "",
  productDescription: "",
  productScale: "",
  productVendor: 0,
  quantityInStock: 0,
  buyPrice: "",
  imgURL: "",
}
// hàm thu thập dữ liệu Product từ form
function getDataProduct(){
  "use strict";
  gProduct.createdAt = moment($("#md-createdAt").val()).toISOString();
  gProduct.productCode = $("#md-productCode").val().trim();
  gProduct.productName = $("#md-productName").val().trim();
  gProduct.buyPrice = $("#md-buyPrice").val().trim();
  gProduct.imgURL =  $("#md-imgURL").val().trim(); 
  gProduct.productDescription = $("#md-productDescription").val()
  gProduct.productScale = $("#md-productScale").val()
  gProduct.quantityInStock = $("#md-quantityInStock").val()
  gProduct.productVendor = $("#md-productVendor").val()
} 
// Hàm bấm nút tạo new Product
function onBtnConfirmCreateProductClick(){ 
  "use strict";
  // 1.thu thập dữ liệu người order
  getDataProduct();
  //2. validate dữ liệu user
  var vIsDataValidated = validateDataProduct();
  if(vIsDataValidated) {
  //3. gọi api lấy danh sách voucher
  callApiCreateProduct();
  $("#Product-create-modal").modal("hide");
  
  }
}
//Hàm gọi api get danh sách Product
function callApiGetProductData(){ 
  "use strict";
  $.ajax({
    url: gBASE_PRODUCT_URL ,
    type: "GET",
    dataType: 'json',
    
    success: function(responseObject){
      //debugger;
      gListProduct.products = responseObject;
      console.log(gListProduct)
      loadProductToTable(gListProduct.products); 
      //loadPhongBanToSelect();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}
function loadProductToTable(paramProducts) {
  gProductTable.clear();
  gProductTable.rows.add(paramProducts);
  gProductTable.draw();
}

//upload img
function onBtnUploadImgProductClick(){
  callApiUploadImg();
  
}

function callApiUploadImg(){
  $.ajax({
    url: 'http://localhost:8080/uploadFile' ,
    type: "POST",
    dataType: 'form-data',
   
    processData: false,
    contentType: false,
    contentType: 'multipart/form-data',
    success: function(responseObject){
    
     // loadDataToModalProduct(responseObject);
   },
    error: function(error){
    console.assert(error.responseText);
    }
  });
}



})