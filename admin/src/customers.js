/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function () {
  "use strict";

  const gBASE_CUSTOMER_URL = "http://localhost:8080/customers/";
  const gBASE_ORDER_URL = "http://localhost:8080/orders";
  const gBASE_PRODUCT_LINE_URL = "http://localhost:8080/product-lines";
  const gBASE_PRODUCT_URL = "http://localhost:8080/products";
  var gCustomerId = "";
  var gListCustomer = {
    customers: [{}],
  };

  let gCustomerTable = $("#example1").DataTable({
    bDestroy: false,
    columns: [
      { data: "id" },
      { data: "firstName" },
      { data: "address" },
      { data: "creditLimit" },
      { data: "phoneNumber" },
      { data: "postalCode" },
      { data: "salesRepEmployeeNumber" },
      { data: "action" },
    ],
    columnDefs: [
      {
        targets: -1,
        defaultContent: `<i id="icon-view" title="View Order" class="fas fa-eye text-warning"></i> |
      <i id="icon-updateCustomer" title="Update Info" class="fas fa-edit text-primary"></i>
      | <i id="icon-delete-customer" title="Delete" class="fas fa-trash text-danger"></i>`,
      },
      {
        targets: 1,
        width: "15%",
        render: function (data, type, row, meta) {
          return row.firstName + " " + row.lastName;
        },
      },
      {
        targets: 2,
        width: "25%",
        render: function (data, type, row, meta) {
          return (
            row.address +
            ", " +
            row.state +
            ", " +
            row.city +
            ", " +
            row.country
          );
        },
      },
      { targets: 3, width: "5%" },
      { targets: 6, width: "5%" },
      { targets: 5, width: "5%" },
      { targets: 0, width: "3%" },
    ],
  });

  //table order/orderdetail
  let gOrderDetailTable = $("#order-detail-table").DataTable({
    columns: [
      { data: "id" },
      { data: "orderDate" },
      { data: "requiredDate" },
      { data: "shippedDate" },
      { data: "status" },
      { data: "comments" },
      { data: "action" },
    ],
    columnDefs: [
      {
        targets: -1,
        defaultContent: `
      <i id="icon-updateOrder" title="Update Info" class="fas fa-edit text-primary"></i>
      | <i id="icon-deleteOrder" title="Delete" class="fas fa-trash text-danger"></i>`,
      },
    ],
  });
  const token = getCookie("token");

  function checkTokenRole() {
    if (!token) {
      window.location.href = "cannotaccess.html";
    } else {
      $.ajax({
        url: "http://localhost:8080/hello3",
        type: "GET",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", "Token " + token);
        },

        success: function () {},
        error: function (res) {
          window.location.href = "cannotaccess.html";
        },
      });
    }
  }

  //Hàm get Cookie
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  var gId = "";
  var gRowData = {};

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  // Export Excel Customer
  // $("#btn-export-customer").on("click", function(){
  //     onBtnExportCustomerDataClick();
  //   })
  //I: lấy Info nhân viên
  // $("#btn-detail-customer").on("click", function(){
  //   onBtnGetInfoNhanVienClick();
  // })

  //C: Create
  $("#btn-add-new").on("click", function () {
    onBtnCreateCustomerClick();
  });
  $(".content").on("click", "#btn-confirm-create", function () {
    onBtnConfirmCreateCustomerClick(this);
  });
  //U: Update
  $("#example1").on("click", "#icon-updateCustomer", function () {
    onIconEditCustomerClick(this);
  });
  $("#customer-edit-modal").on("click", "#btn-confirm-edit", function () {
    onBtnConfirmUpdateClick(this);
  });

  //D: Delete customer
  $("#example1").on("click", "#icon-delete-customer", function () {
    onIconDeleteCustomerClick(this);
  });
  $("#btn-confirm-delete-customer").on(
    "click",
    onBtnConfirmDeleteCustomerClick
  );
  //Button view order
  $("#example1").on("click", "#icon-view", function () {
    onIconViewOrderClick(this);
  });

  //====================================================================
  // Order Action
  //====================================================================
  //C: Create order
  $("#btn-add-newOrder").on("click", function () {
    onBtnCreateOrderClick();
  });
  $(".content").on("click", "#btn-confirm-create-order", function () {
    onBtnConfirmCreateOrderClick(this);
  });
  //U: Update order
  $("#order-detail-table").on("click", "#icon-updateOrder", function () {
    onIconEditOrderClick(this);
  });
  $("#order-edit-modal").on("click", "#btn-confirm-edit-order", function () {
    onBtnConfirmUpdateOrderClick(this);
  });
  //D: Delete order
  $("#order-detail-table").on("click", "#icon-deleteOrder", function () {
    onIconDeleteOrderClick(this);
  });

  $("#btn-confirm-delete-order").on("click", function () {
    onBtnConfirmDeleteOrderClick();
  });

  // hàm xử lý sự kiện goi api delete order
  function callApiDeleteOrder() {
    "use strict";
    $.ajax({
      url: gBASE_ORDER_URL + "/" + gOrderId,
      type: "DELETE",
      dataType: "json",
      success: function (responseObject) {
        alert("Delete order Success !");
        callApiGetOrderByCustomerId();
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  function onBtnConfirmDeleteOrderClick() {
    debugger;
    ("use strict");
    console.log("sss");
    callApiDeleteOrder();
    // hiển thị lại giao diện
    $("#modal-delete-order-confirm").modal("hide");
  }

  // hàm xử lý sự kiện click icon delete
  function onIconDeleteOrderClick(paramDeleteIcon) {
    "use strict";
    var vDataRow = getIdFromIconInOrderTable(paramDeleteIcon);
    gOrderId = vDataRow.id;
    $("#modal-delete-order-confirm").modal("show");
  }

  var gOrderId = "";
  var gOrderObj = {};
  var gListOrders = {
    orders: [{}],
  };

  // hàm xử lý sự kiện goi api update order
  function callApiUpdateOrder(gOrderObj) {
    "use strict";
    $.ajax({
      url: gBASE_ORDER_URL + "/" + gOrderId,
      type: "PUT",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(gOrderObj),
      success: function (responseObject) {
        $("#order-edit-modal").modal("hide");
        callApiGetOrderByCustomerId();
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }
  // hàm validate dữ liệu các trường input
  // function validateDataOrder(){
  //   "use strict";
  //     if(gOrderObj.orderDate =="" )  {
  //       alert("Vui lòng nhập orderDate");
  //       return false;
  //     }
  //     if(gOrderObj.requiredDate == "")  {
  //       alert("Vui lòng nhập requiredDate");
  //       return false;
  //     }
  //     if(gOrderObj.shippedDate === "")  {
  //       alert("Vui lòng nhập shippedDate");
  //       return false;
  //     }
  //     else {return true;}
  // }
  // hàm thu thập dữ liệu order từ form
  function getDataOrderUpdate() {
    "use strict";
    gOrderObj.orderDate = moment($("#md-orderDate-edit").val()).toISOString();
    gOrderObj.requiredDate = moment(
      $("#md-requiredDate-edit").val()
    ).toISOString();
    gOrderObj.shippedDate = moment(
      $("#md-shippedDate-edit").val()
    ).toISOString();
    gOrderObj.status = $("#md-status-edit").val().trim();
    gOrderObj.comments = $("#md-comments-edit").val().trim();
  }
  // hàm xử lý sự kiện nút confirm edit order
  function onBtnConfirmUpdateOrderClick() {
    // 1.thu thập dữ liệu order
    getDataOrderUpdate();
    //2. validate dữ liệu user
    // var vIsDataValidated = validateDataOrder();
    // if(vIsDataValidated) {
    //3. gọi api update
    // }
    callApiUpdateOrder(gOrderObj);
    $("#order-edit-modal").modal("hide");
  }
  // click icon update order
  function onIconEditOrderClick(paramEditIcon) {
    "use strict";
    //lưu thông tin id, orderid vào biến toàn cục
    var vDataRow = getIdFromIconInOrderTable(paramEditIcon);
    gOrderId = vDataRow.id;

    $("#order-edit-modal").modal("show");

    callApiGetOrderById();
  }

  // hàm hiển thị order lên modal edit
  function loadDataToModalOrder(responseObject) {
    "use strict";
    //Fill các trường input
    var dateOrder = moment(responseObject.orderDate).utc().format("YYYY-MM-DD");
    var dateRequired = moment(responseObject.requiredDate)
      .utc()
      .format("YYYY-MM-DD");
    var dateShipped = moment(responseObject.shippedDate)
      .utc()
      .format("YYYY-MM-DD");
    debugger;
    $("#md-customerId-edit2").val(gCustomerId);
    $("#md-orderDate-edit").val(dateOrder);
    $("#md-requiredDate-edit").val(dateRequired);
    $("#md-shippedDate-edit").val(dateShipped);
    $("#md-status-edit ").val(responseObject.status).change();
    $("#md-comments-edit").val(responseObject.comments);
  }

  // hàm gọi api get order by id
  function callApiGetOrderById() {
    "use strict";
    $.ajax({
      url: gBASE_ORDER_URL + "/" + gOrderId,
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        loadDataToModalOrder(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  // click create
  function onBtnCreateOrderClick() {
    "use strict";
    $("#order-create-modal").modal("show");
    $("#md-customerId").val(gId);
    callApiGetDataProductLine();
  }

  //load product/productline
  var gObjectDataProductLine = {};

  function callApiGetDataProductLine() {
    "use strict";
    $.ajax({
      url: gBASE_PRODUCT_LINE_URL,
      type: "GET",
      dataType: "json",

      success: function (responseObject) {
        gObjectDataProductLine = responseObject;
        loadDataProductLineToSelect(gObjectDataProductLine);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  function loadDataProductLineToSelect(paramgObjectDataProductLine) {
    "use strict";
    var vSelect = $("#md-productLine");
    vSelect.empty();
    $("<option/>", {
      val: 0,
      text: "Select Product Line",
    }).appendTo(vSelect);
    for (var bI = 0; bI < paramgObjectDataProductLine.length; bI++) {
      $("<option/>", {
        val: paramgObjectDataProductLine[bI].id,
        text: paramgObjectDataProductLine[bI].productLine,
      }).appendTo(vSelect);
    }
    vSelect.on("change", function () {
      onSelectProductLineChange(vSelect);
    });
  }

  function onSelectProductLineChange(vSelect) {
    var vProductLineId = vSelect.val();

    $.ajax({
      url: gBASE_PRODUCT_URL + "/product-lines/" + vProductLineId,
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        loadDataProductToSelect(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  function loadDataProductToSelect(responseObject) {
    var vProductSelect = $("#md-productName");
    console.log(responseObject);
    vProductSelect.empty();
    $("<option/>", {
      val: 0,
      text: "Select Product",
    }).appendTo(vProductSelect);

    if (responseObject.length == 0) {
      vProductSelect.disabled = true;
    } else {
      vProductSelect.disabled = false;
      for (var bI = 0; bI < responseObject.length; bI++) {
        $("<option/>", {
          val: responseObject[bI].id,
          text: responseObject[bI].productName,
        }).appendTo(vProductSelect);
      }
    }
  }

  // Hàm bấm nút tạo new order
  function onBtnConfirmCreateOrderClick() {
    "use strict";
    // 1.thu thập dữ liệu  order
    getDataOrder();
    // //2. validate dữ liệu
    // var vIsDataValidated = validateDataOrder();
    // if(vIsDataValidated) {
    //3. gọi api lấy danh sách order
    callApiCreateOrder();
    $("#order-create-modal").modal("hide");
  }
  function callApiCreateOrder() {
    "use strict";
    // Khai báo object gửi lên api create order
    var vObjectRequest = {
      orderDate: "",
      requiredDate: "",
      shippedDate: "",
      status: "",
      comments: "",
    };

    vObjectRequest = gOrderObj;

    $.ajax({
      url: gBASE_ORDER_URL + "/" + gCustomerId,
      dataType: "json",
      type: "POST",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(vObjectRequest),
      success: function (responseObject) {
        callApiGetOrderByCustomerId();
        alert("Create Order Success!");
      },
      error: function (error) {
        //console.assert(error.responseText);
      },
    });
    //console.log(gReponseObj);
  }
  // hàm validate dữ liệu các trường input
  function validateDataOrder() {
    "use strict";
    if (gOrderObj.orderDate == "") {
      alert("Vui lòng nhập orderDate");
      return false;
    }
    if (gOrderObj.requiredDate == "") {
      alert("Vui lòng nhập requiredDate");
      return false;
    }
    if (gOrderObj.shippedDate === "") {
      alert("Vui lòng nhập shippedDate");
      return false;
    } else {
      return true;
    }
  }
  // Khai báo đối tượng Order
  var gOrderObj = {
    orderDate: "",
    requiredDate: "",
    shippedDate: "",
    status: "",
    comments: "",
    orderDetails: [
      {
        productId: {
          productCode: "",
          productName: "",
          productDescription: "",
        },
      },
    ],
  };
  // hàm thu thập dữ liệu order từ form
  function getDataOrder() {
    "use strict";
    gOrderObj.orderDate = moment($("#md-orderDate").val()).toISOString();
    gOrderObj.requiredDate = moment($("#md-requiredDate").val()).toISOString();
    gOrderObj.shippedDate = moment($("#md-shippedDate").val()).toISOString();
    gOrderObj.status = $("#md-status").val().trim();
    gOrderObj.comments = $("#md-comments").val().trim();
    gCustomerId = $("#md-customerId").val().trim();
  }
  // Hàm bấm nút tạo new order
  function onBtnConfirmCreateOrderClick() {
    "use strict";
    // 1.thu thập dữ liệu  order
    getDataOrder();
    // //2. validate dữ liệu
    // var vIsDataValidated = validateDataOrder();
    // if(vIsDataValidated) {
    //3. gọi api lấy danh sách order
    callApiCreateOrder();
    $("#order-create-modal").modal("hide");
  }

  function onIconViewOrderClick(paramViewIcon) {
    //lưu thông tin id, orderid vào biến toàn cục
    var vDataRow = getIdFromIcon(paramViewIcon);
    gId = vDataRow.id;
    $("#orderDetail-modal").modal("show");
    callApiGetOrderByCustomerId();
  }

  // hàm gọi api get order by  customer id
  function callApiGetOrderByCustomerId() {
    "use strict";
    $.ajax({
      url: gBASE_ORDER_URL + "/customers/" + gId,
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        //console.log(responseObject)
        loadOrderDetailToTable(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  function loadOrderDetailToTable(responseObject) {
    gOrderDetailTable.clear();
    gOrderDetailTable.rows.add(responseObject);
    gOrderDetailTable.draw();
  }
  // $("#modal-delete-all-confirm").on("click", "#btn-confirm-delete-all", function() {
  //   onBtnConfirmDeleteAllClick(this);
  // });
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  onPageLoading();

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  //Hàm load trang
  function onPageLoading() {
    "use strict";
    checkTokenRole();
    callApiGetCustomerData();
  }

  // hàm xử lý sự kiện goi api delete order
  function callApiDeleteCustomer() {
    "use strict";
    $.ajax({
      url: gBASE_CUSTOMER_URL + gCustomerId,
      type: "DELETE",
      dataType: "json",
      success: function () {
        alert("Delete Customer thành công !");
        callApiGetCustomerData();
      },
      error: function (error) {
        alert(" Error: Customer hiện đang có order");
      },
    });
  }
  //Xoá order từ gID của customer
  function deleteCustomer() {
    "use strict";
    callApiDeleteCustomer();
  }
  /**
   * Hàm xử lý khi click confirm
   * Input: paramIconDelete icon click
   * Output: xóa được order và load table
   */
  function onBtnConfirmDeleteCustomerClick() {
    "use strict";
    deleteCustomer();
    // hiển thị lại giao diện

    $("#modal-delete-customer-confirm").modal("hide");
  }
  // hàm xử lý sự kiện click icon delete
  function onIconDeleteCustomerClick(paramDeleteIcon) {
    "use strict";

    //lưu thông tin id, orderid vào biến toàn cục
    var vDataRow = getIdFromIcon(paramDeleteIcon);
    gCustomerId = vDataRow.id;

    $("#modal-delete-customer-confirm").modal("show");
  }
  //
  function onBtnCreateCustomerClick() {
    "use strict";
    $("#customer-create-modal").modal("show");
  }

  // hàm xử lý sự kiện goi api update customer
  function callApiUpdateCustomer(gCustomer) {
    "use strict";
    $.ajax({
      url: gBASE_CUSTOMER_URL + gId,
      type: "PUT",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(gCustomer),
      success: function (responseObject) {
        $("#customer-edit-modal").modal("hide");
        callApiGetCustomerData();
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }
  // hàm validate dữ liệu các trường input
  function validateDataCustomerUpdate() {
    "use strict";
    if (gCustomer.firstName == "") {
      alert("Vui lòng nhập Firstname");
      return false;
    }
    if (gCustomer.lastName == "") {
      alert("Vui lòng nhập Lastname");
      return false;
    }
    if (gCustomer.phoneNumber === "") {
      alert("Vui lòng nhập Phonenumber");
      return false;
    } else {
      return true;
    }
  }
  // hàm thu thập dữ liệu customer từ form
  function getDataCustomerUpdate() {
    "use strict";
    gCustomer.firstName = $("#md-firstname-edit").val().trim();
    gCustomer.lastName = $("#md-lastname-edit").val().trim();
    gCustomer.address = $("#md-address-edit").val().trim();
    gCustomer.state = $("#md-state-edit").val().trim();
    gCustomer.country = $("#md-country-edit").val().trim();
    gCustomer.city = $("#md-city-edit").val();
    gCustomer.phoneNumber = $("#md-phone-edit").val();
    gCustomer.postalCode = $("#md-postalCode-edit").val();
    gCustomer.creditLimit = $("#md-creditLimit-edit").val();
    gCustomer.salesRepEmployeeNumber = $("#md-saleNum-edit").val();
  }
  // hàm xử lý sự kiện nút confirm edit customer
  function onBtnConfirmUpdateClick() {
    // 1.thu thập dữ liệu customer
    getDataCustomerUpdate();
    //2. validate dữ liệu user
    var vIsDataValidated = validateDataCustomerUpdate();
    if (vIsDataValidated) {
      //3. gọi api update
      callApiUpdateCustomer(gCustomer);
      $("#customer-edit-modal").modal("hide");
    }
  }
  // hàm hiển thị customer lên modal edit
  function loadDataToModalCustomer(responseObject) {
    ("use strict");
    //Fill các trường input
    $("#md-firstname-edit").val(responseObject.firstName);
    $("#md-lastname-edit").val(responseObject.lastName);
    $("#md-address-edit").val(responseObject.address);
    $("#md-state-edit").val(responseObject.state);
    $("#md-country-edit").val(responseObject.country);
    $("#md-city-edit").val(responseObject.city);
    $("#md-phone-edit").val(responseObject.phoneNumber);
    $("#md-postCode-edit").val(responseObject.postalCode);
    $("#md-creditLimit-edit").val(responseObject.creditLimit);
    $("#md-saleNum-edit").val(responseObject.salesRepEmployeeNumber);
  }
  // hàm gọi api get customer by id
  function callApiGetCustomerById() {
    "use strict";
    $.ajax({
      url: gBASE_CUSTOMER_URL + gCustomerId,
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        loadDataToModalCustomer(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }
  // hàm thu thập dữ liệu id và orderid
  function getIdFromIconInOrderTable(paramEditIcon) {
    "use strict";
    var vTableRow = $(paramEditIcon).parents("tr");
    var vOrderRowData = gOrderDetailTable.row(vTableRow).data();
    return vOrderRowData;
  }

  // hàm thu thập dữ liệu id customer
  function getIdFromIcon(paramEditIcon) {
    "use strict";
    var vTableRow = $(paramEditIcon).parents("tr");
    var vCustomerRowData = gCustomerTable.row(vTableRow).data();
    return vCustomerRowData;
  }
  // hàm xử lý sự kiện click icon edit
  function onIconEditCustomerClick(paramEditIcon) {
    "use strict";

    // // update trạng thái form
    // gFormMode = gFORM_MODE_UPDATE;
    // $("#div-form-mod").html(gFormMode);
    //lưu thông tin id, orderid vào biến toàn cục
    var vDataRow = getIdFromIcon(paramEditIcon);
    gCustomerId = vDataRow.id;

    $("#customer-edit-modal").modal("show");

    callApiGetCustomerById();
  }
  //
  function callApiCreateCustomer() {
    "use strict";
    // Khai báo object gửi lên api create customer
    var vObjectRequest = {
      firstName: "",
      lastName: "",
      phoneNumber: "",
      postalCode: "",
      salesRepEmployeeNumber: 0,
      creditLimit: 0,
      address: "",
      state: "",
      city: "",
      country: "",
    };

    vObjectRequest = gCustomer;

    $.ajax({
      url: gBASE_CUSTOMER_URL,
      dataType: "json",
      type: "POST",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(vObjectRequest),
      success: function (responseObject) {
        callApiGetCustomerData();
      },
      error: function (error) {
        alert("Số điện thoại đã tồn tại trong hệ thống");
      },
    });
  }

  
  // hàm validate dữ liệu các trường input
  function validateDataCustomer() {
    "use strict";
    if (gCustomer.firstName == "") {
      alert("Vui lòng nhập Firstname");
      return false;
    }
    if (gCustomer.lastName == "") {
      alert("Vui lòng nhập Lastname");
      return false;
    }
    if (gCustomer.phoneNumber === "") {
      alert("Vui lòng nhập Phonenumber");
      return false;
    } 
    
    else {
      return true;
    }
  }
  // Khai báo đối tượng Person
  var gCustomer = {
    firstName: "",
    lastName: "",
    phoneNumber: "",
    postalCode: "",
    salesRepEmployeeNumber: 0,
    creditLimit: 0,
    address: "",
    state: "",
    city: "",
    country: "",
  };
  // hàm thu thập dữ liệu customer từ form
  function getDataCustomer() {
    "use strict";
    gCustomer.firstName = $("#md-firstname").val().trim();
    gCustomer.lastName = $("#md-lastname").val().trim();
    gCustomer.address = $("#md-address").val().trim();
    gCustomer.state = $("#md-state").val().trim();
    gCustomer.country = $("#md-country").val().trim();
    gCustomer.city = $("#md-city").val();
    gCustomer.phoneNumber = $("#md-phone").val();
    gCustomer.postalCode = $("#md-postalCode").val();
    gCustomer.creditLimit = $("#md-creditLimit").val();
    gCustomer.salesRepEmployeeNumber = $("#md-saleNum").val();
  }
  // Hàm bấm nút tạo new customer
  function onBtnConfirmCreateCustomerClick() {
    "use strict";
    // 1.thu thập dữ liệu người order
    getDataCustomer();
    //2. validate dữ liệu user
    var vIsDataValidated = validateDataCustomer();
    if (vIsDataValidated) {
      //3. gọi api lấy danh sách voucher
      callApiCreateCustomer();
      $("#customer-create-modal").modal("hide");
    }
  }
  //Hàm gọi api get danh sách customer
  function callApiGetCustomerData() {
    "use strict";
    $.ajax({
      url: gBASE_CUSTOMER_URL,
      type: "GET",
      dataType: "json",

      success: function (responseObject) {
        //debugger;
        gListCustomer.customers = responseObject;
        console.log(gListCustomer);
        loadCustomerToTable(gListCustomer.customers);
        //loadPhongBanToSelect();
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }
  function loadCustomerToTable(paramCustomer) {
    gCustomerTable.clear();
    gCustomerTable.rows.add(paramCustomer);
    gCustomerTable.draw();
  }

  // $("#example1").DataTable({
  //   "responsive": true, "lengthChange": false, "autoWidth": false,"bDestroy":true,
  //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
});
