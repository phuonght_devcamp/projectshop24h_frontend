jQuery(document).ready(function () {
  checkCookieExist();
  function checkCookieExist() {
    var myCookie = getCookie("token");

    if (myCookie !== "") {
      // do cookie doesn't exist stuff;
      $("#nav_login").remove();
      $("#menu_link").append(
        '<li id="nav_login"><i class="fa fa-user"></i> <span id="logout">Logout</span></li>'
      );
    } else {
      window.location.href = "login.html";
    }
  }

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function deleteCookie(name) {
    document.cookie =
      name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }

  $("#logout").on("click", function () {
    deleteCookie("token");
    window.location.href = "index.html";
  });
  // jQuery sticky Menu
  callApiGetProductData();
  $(".mainmenu-area").sticky({ topSpacing: 0 });

  $(".product-carousel").owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 5,
      },
    },
  });

  $(".related-products-carousel").owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 2,
      },
      1000: {
        items: 2,
      },
      1200: {
        items: 3,
      },
    },
  });

  $(".brand-list").owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 4,
      },
    },
  });

  // Bootstrap Mobile Menu fix
  $(".navbar-nav li a").click(function () {
    $(".navbar-collapse").removeClass("in");
  });

  // jQuery Scroll effect
  $(".navbar-nav li a, .scroll-to-up").bind("click", function (event) {
    var $anchor = $(this);
    var headerH = $(".header-area").outerHeight();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $($anchor.attr("href")).offset().top - headerH + "px",
        },
        1200,
        "easeInOutExpo"
      );

    event.preventDefault();
  });

  // Bootstrap ScrollPSY
  $("body").scrollspy({
    target: ".navbar-collapse",
    offset: 95,
  });

  // --------------------------------------------------------------------------------------------------
  var gProductDB = {};

  // Get data product

  function callApiGetProductData() {
    "use strict";
    $.ajax({
      url: "http://localhost:8080/products",
      type: "GET",
      dataType: "json",
      async: false,
      success: function (responseObject) {
        gProductDB = responseObject;
        loadProductToIndexHTML(gProductDB);
        loadProductToShopHTML(gProductDB);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  function loadProductToIndexHTML() {
    console.log(gProductDB);
    var vProductSection = ``;
    for (var bI = 0; bI < gProductDB.length; bI++) {
      var vProductSection =
        `
       
        <div class="single-product">
            <div class="product-f-image">
                <img src="` +
        gProductDB[bI].imgURL +
        `" alt="">
                <div class="product-hover">
                    <a href="#" class="add-to-cart-link" data-img="` +
        gProductDB[bI].imgURL +
        `" data-name="` +
        gProductDB[bI].productName +
        `" data-price="` +
        gProductDB[bI].buyPrice +
        `"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    <a href="#" class="view-details-link" data-id="` +
        gProductDB[bI].id +
        `"><i class="fa fa-link"></i> See details</a>
                </div>
            </div>
            
            <h2><a href="single-product.html">` +
        gProductDB[bI].productName +
        `</a></h2>
            
            <div class="product-carousel-price">
                <ins>$` +
        gProductDB[bI].buyPrice +
        `</ins> 
            </div> 
        </div>
        
         `;
      $(".product-carousel ").append(vProductSection);
    }
  }

  function loadProductToShopHTML() {
    //console.log(gProductDB)
    var vProductSection = ``;
    for (var bI = 0; bI < gProductDB.length; bI++) {
      var vProductSection =
        `
       
        <div class="col-md-3 col-sm-6">
            <div class="single-shop-product">
                <div class="product-upper">
                    <img src="` +
        gProductDB[bI].imgURL +
        `" alt="">
                </div>
                <h2><a href="#">` +
        gProductDB[bI].productName +
        `</a></h2>
                <div class="product-carousel-price">
                    <ins>$` +
        gProductDB[bI].buyPrice +
        `</ins> 
                </div>  
                
                <div class="product-option-shop">
                    <a class="add_to_cart_button" data-img="` +
        gProductDB[bI].imgURL +
        `" data-name="` +
        gProductDB[bI].productName +
        `" data-price="` +
        gProductDB[bI].buyPrice +
        `">Add to cart</a>
                </div>                       
            </div>
        </div>
        
         `;
      $(".product-section ").append(vProductSection);
    }
  }

  var gProductObj = {};

  $(".view-details-link").on("click", function () {
    var vId = $(this).data("id");
    sessionStorage.setItem("productId", vId);
    //console.log(gProductObj)
    window.location.href = "single-product.html";
  });

  // Checkout JS
  // =============================

  //Place Order
  $("#place_order").on("click", function (e) {
    e.preventDefault();
    onBtnPlaceOrderClick();
  });

  var gBASE_CUSTOMER_URL = "http://localhost:8080/customers";
  var gBASE_ORDER_URL = "http://localhost:8080/orders";
  var gBASE_PAYMENT_URL = "http://localhost:8080/payments";

  var gCustomerObj = {};
  var gCustomerId = "";
  var gOrderObj = {
    id: "",
    orderDate: "",
    comments: "",
    requiredDate: "",
    shippedDate: "",
    customerId: {
      id: "",
      lastName: "",
      firstName: "",
      phoneNumber: "",
    },
  };

  function onBtnPlaceOrderClick() {
    getDataCustomer();
    // validate
    var vIsDataValidated = validateDataCustomer();
    if (vIsDataValidated) {
        // post api
        callApiGetCustomerByPhone();
    }
  }
  const gBASE_ORDER_DETAIL_URL = "http://localhost:8080/order-details";

  function callApiGetCustomerByPhone() {
    $.ajax({
      url: gBASE_CUSTOMER_URL + "/phone/" + gCustomerObj.phoneNumber,
      dataType: "json",
      type: "GET",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle

      success: function (responseObject) {
        //console.log(responseObject[0].id)

        if (responseObject.length != 0) {
          callApiUpdateCustomer(responseObject[0].id);
        } else callApiSaveCustomer();
      },
      error: function (error) {},
    });
  }

  function callApiUpdateCustomer(paramCustomerId) {
    $.ajax({
      url: gBASE_CUSTOMER_URL + "/" + paramCustomerId,
      dataType: "json",
      type: "PUT",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(gCustomerObj),
      success: function (responseObject) {
        gCustomerId = responseObject.id;

        callApiSaveOrder(gCustomerId);
      },
      error: function (error) {},
    });
  }
  function callApiSaveCustomer() {
    $.ajax({
      url: gBASE_CUSTOMER_URL,
      dataType: "json",
      type: "POST",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(gCustomerObj),
      success: function (responseObject) {
        gCustomerId = responseObject.id;

        callApiSaveOrder(gCustomerId);
      },
      error: function (error) {},
    });
  }

  function callApiSaveOrder() {
    $.ajax({
      url: gBASE_ORDER_URL + "/" + gCustomerId,
      dataType: "json",
      type: "POST",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(gOrderObj),
      success: function (responseObject) {
        var vOrderId = responseObject.id;

        //console.log(gOrderObj);
        console.log("Create Order Success!");
        callApiCreateOrderDetail(vOrderId);
        callApiCreatePayment(vOrderId);
      },
      error: function (error) {},
    });
  }
  var gPaymentObj = {};

  function callApiCreatePayment(vOrderId) {
    gPaymentObj.paymentDate = getCurrentDate();
    gPaymentObj.checkNumber = "CHECKNO" + gCustomerId + "-" + vOrderId;

    $.ajax({
      url: gBASE_PAYMENT_URL + "/" + gCustomerId,
      dataType: "json",
      type: "POST",
      contentType: "application/json",
      async: false, // tham số async nhận giá trị fasle
      data: JSON.stringify(gPaymentObj),
      success: function (responseObject) {
        console.log("Create Payment Success!");
        $("#modal-success-order").modal("show");
      },
      error: function (error) {},
    });
  }

  function callApiCreateOrderDetail(vOrderId) {
    var vOrderDetail = getDataOrderDetail();

    for (var i = 0; i < vOrderDetail.length; i++) {
      $.ajax({
        url:
          gBASE_ORDER_DETAIL_URL +
          "/orders/" +
          vOrderId +
          "/products/" +
          vOrderDetail[i].productId.id,
        dataType: "json",
        type: "POST",
        contentType: "application/json",
        async: false, // tham số async nhận giá trị fasle
        data: JSON.stringify(vOrderDetail[i]),
        success: function (responseObject) {
          //console.log(gOrderObj);
          console.log("Create Order DEtail Success!");
        },
        error: function (error) {},
      });
    }
  }

  
  function ValidateEmail() {
    if (
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        $("#billing_email").val()
      )
    ) {
      return true;
    }
    return false;
  }

  function validateDataCustomer() { 
    "use strict";
    if (gCustomerObj.firstName == "") {
      alert("Please fill firstName input");
      return false;
    }

    if (gCustomerObj.lastName == "") {
      alert("Please fill in lastName input");
      return false;
    }
    if (gCustomerObj.email == "") {
      alert("Please fill  in email");
      return false;
    }
    if (!ValidateEmail()) {
        alert("You have entered an invalid email address!");
      return false;
    }

    if (gCustomerObj.phoneNumber == "") {
      alert("Please fill  in phoneNumber input");
      return false;
    }
    if (gCustomerObj.address == "") {
      alert("Please fill  in address input");
      return false;
    }
    if (gCustomerObj.city == "") {
      alert("Please fill  in city input");
      return false;
    }
    if (gCustomerObj.state == "") {
      alert("Please fill  in state input");
      return false;
    }
    if (gCustomerObj.postalCode == "") {
      alert("Please fill  in postcode input");
      return false;
    } else {
      return true;
    }
  }

  function getDataCustomer() {
    gCustomerObj.country = $("#billing_country").val();
    gCustomerObj.address = $("#billing_address_1").val().trim();
    gCustomerObj.city = $("#billing_city").val().trim();
    gCustomerObj.state = $("#billing_state").val().trim();
    gCustomerObj.postalCode = $("#billing_postcode").val().trim();
    gCustomerObj.firstName = $("#billing_first_name").val().trim();
    gCustomerObj.lastName = $("#billing_last_name").val().trim();
    gCustomerObj.email = $("#billing_email").val().trim();
    gCustomerObj.phoneNumber = $("#billing_phone").val().trim();
    gCustomerObj.salesRepEmployeeNumber = $("#billing_sale_rep").val().trim();
    gOrderObj.status = "Waiting";
    gOrderObj.orderDate = getCurrentDate();
    gOrderObj.requiredDate = getRequiredDate();
    gOrderObj.comments = $("#billing_comment").val().trim();
  }

  function getDataOrderDetail() {
    let gOrderDetail = Array.from({ length: cart.length }, () => ({
      quantityOrder: "",
      priceEach: "",
      productId: { id: "" },
    }));

    var totalCart = 0;
    if (cart.length == 0) {
      alert("Cart is empty!");
    } else {
      for (var i = 0; i < cart.length; i++) {
        gOrderDetail[i].priceEach = cart[i].price;
        gOrderDetail[i].quantityOrder = cart[i].count;
        gOrderDetail[i].productId.id = cart[i].id;
        totalCart += cart[i].price * cart[i].count;
      }
      gPaymentObj.amount = totalCart;
      return gOrderDetail;
    }
  }

  function getCurrentDate() {
    var today = new Date();
    // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    // var dateTime = date+' '+time;
    return today.toISOString();
  }

  function getRequiredDate() {
    var requiredDate = new Date(Date.now() + 3600 * 1000 * 24);
    // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+(today.getDate()+3);
    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    // var dateTime = date+' '+time;
    return requiredDate.toISOString();
  }
});

(function (i, s, o, g, r, a, m) {
  i["GoogleAnalyticsObject"] = r;
  (i[r] =
    i[r] ||
    function () {
      (i[r].q = i[r].q || []).push(arguments);
    }),
    (i[r].l = 1 * new Date());
  (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m);
})(
  window,
  document,
  "script",
  "https://www.google-analytics.com/analytics.js",
  "ga"
);

ga("create", "UA-10146041-21", "auto");
ga("send", "pageview");
