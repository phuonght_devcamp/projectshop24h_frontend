$(document).ready(function(event){
    event.preventDefault;
    var gProDuctLineDB={}
    var gProductFilter={};
    var gProductDB={};

    onPageLoading();
    function onPageLoading(){
        callApiGetProductLine();
        callApiGetProductData();
    }
 

    $('#button-addon4').on('click',function(){searchProduct();})
    $('#tab-all').on('click',function(){onTabAllProductClick()})
    $('#tab-hot').on('click',function(){onTabHotProductClick()})
    $('.tablinks').on('click',function(){ 
        $('#flexRadioDefault4').prop('checked',true) 

    })
    $('#tab-latest').on('click',function(){onTabLatestProductClick(event)})
    $('#btn-productLine-1').on('click', function(){onTabProductLineClick(this)})
    $('#btn-productLine-2').on('click', function(){onTabProductLineClick(this)})
    $('#btn-productLine-3').on('click', function(){onTabProductLineClick(this)})

    function loadProductLineToMenuTab(){
        for(var i=0;i<gProDuctLineDB.length;i++){
            var vProductLineSection = `<a id="btn-productLine-`
            +gProDuctLineDB[i].id+`" class="btn-category" data-id="`
            +gProDuctLineDB[i].id+`">`
            +gProDuctLineDB[i].productLine+`</a>`;
            $(".vertical-menu ").append(vProductLineSection)
        }
    }

function callApiGetProductLine(){
    "use strict";
    $.ajax({
    url: 'http://localhost:8080/product-lines' ,
    type: "GET",
    dataType: 'json',
    async:false,
    success: function(responseObject){
        gProDuctLineDB=responseObject;
       loadProductLineToMenuTab();
    },
    error: function(error){
        console.assert(error.responseText);
    }
    });
}

function loadProductFilterToShopHTML(gProDuctFilter){ 
    //console.log(gProductDB)
    $(".product-area ").empty();
    var vProductSection=``;
    for(var bI =0; bI < gProDuctFilter.length; bI++ ){
    var vProductSection = `
    <div class="col-xl-3 col-md-4 col-sm-6 col-xs-6  mt-3 item">
        <div class="single-shop-product ">
            <div class="card1">
                <div class="product-upper" >
                    <img src="`+gProDuctFilter[bI].imgURL+`" alt="" width="70%">
                </div>
                <h6><a >`+gProDuctFilter[bI].productName+`</a></h6>
                <div class="product-carousel-price ">
                    <a class="filterPrice" data-price="`+gProDuctFilter[bI].buyPrice+`">$`+gProDuctFilter[bI].buyPrice+`</a> 
                </div>  
                <div class="product-option-shop">
                    <a class="add_to_cart_button" data-id="`+gProDuctFilter[bI].id+`" data-img="`+gProDuctFilter[bI].imgURL+`" data-name="`+gProDuctFilter[bI].productName+`" data-price="`+gProDuctFilter[bI].buyPrice+`">Add to cart</a>
                </div>  
            </div>                      
        </div>
    </div>
 `
     ;
     $(".product-area ").append(vProductSection);
     loadPagination();
    }
}
function searchProduct(){ 

    var input = $('#myInput').val();
    $.ajax({
        url: 'http://localhost:8080/products/product-name/' + input ,
        type: "GET",
        dataType: 'json',
        async:false,
        success: function(responseObject){
        
            gProductFilter=responseObject;
         //  console.log(gProDuctDB);
           $(".product-area ").empty();
           loadProductFilterToShopHTML(gProductFilter);
        
        },
        error: function(error){
            console.assert(error.responseText);
        }
        });
}

function loadAllProductToShopHTML(){
    $(".product-area ").empty();
    //console.log(gProductDB)
    var vProductSection=``;
    for(var bI =0; bI < gProductDB.length; bI++ ){
    var vProductSection = `
        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-6  mt-3 item">
            <div class="single-shop-product ">
                <div class="card1">
                    <div class="product-upper" >
                        <img src="`+gProductDB[bI].imgURL+`" alt="" width="70%">
                    </div>
                    <h6><a >`+gProductDB[bI].productName+`</a></h6>
                    <div class="product-carousel-price ">
                        <a class="filterPrice" data-price="`+gProductDB[bI].buyPrice+`">$`+gProductDB[bI].buyPrice+`</a> 
                    </div>  
                    <div class="product-option-shop">
                        <a class="add_to_cart_button" data-id="`+gProductDB[bI].id+`" data-img="`+gProductDB[bI].imgURL+`" data-name="`+gProductDB[bI].productName+`" data-price="`+gProductDB[bI].buyPrice+`">Add to cart</a>
                    </div>  
                </div>                      
            </div>
        </div>
     `
     ;
     $(".product-area ").append(vProductSection)
    }
}

function onTabAllProductClick(){ 
    $('#tab-all').addClass('active')
    $('#tab-latest').removeClass('active')
    $('#tab-hot').removeClass('active')
    callApiGetProductData();
    loadPagination();
    removeActiveClassBtn();
}

function onTabHotProductClick(){ ;
    $('#tab-all').removeClass('active')
    $('#tab-latest').removeClass('active')
    $('#tab-hot').addClass('active')
    callApiGetHotProductData();
    loadPagination();
    removeActiveClassBtn();
}

function onTabLatestProductClick(event){ 
    
    event.preventDefault;
    $('#tab-all').removeClass('active')
    $('#tab-latest').addClass('active')
    $('#tab-hot').removeClass('active')
    callApiGetLatestProductData();
    loadPagination();
    removeActiveClassBtn();
}



function callApiGetProductData(){
    "use strict";
    $.ajax({
    url: 'http://localhost:8080/products' ,
    type: "GET",
    dataType: 'json',
    async:false,
    success: function(responseObject){
    
    gProductDB=responseObject;
   
    loadAllProductToShopHTML(gProductDB);
    loadPagination();
    },
    error: function(error){
        console.assert(error.responseText);
    }
    });
}

function onTabProductLineClick(paramButton){  
    $('.vertical-menu a').removeClass('active')
    $(paramButton).addClass('active')
    var vProductLineId = $(paramButton).data('id');
    callApiGetProductByProductLineId(vProductLineId);
}
    

function callApiGetProductByProductLineId(vProductLineId){

    $.ajax({
        url: 'http://localhost:8080/products/product-lines/' + vProductLineId ,
        type: "GET",
        dataType: 'json',
        async:false,
        success: function(responseObject){
            loadProductFilterToShopHTML(responseObject);
            
        },
        error: function(error){
            console.assert(error.responseText);
        }
        });
}


function callApiGetHotProductData(){
    
    $.ajax({
        url: 'http://localhost:8080/products/top10' ,
        type: "GET",
        dataType: 'json',
        async:false,
        success: function(responseObject){
        
            getDataHotProduct(responseObject);
       
            loadProductFilterToShopHTML(gProductHot);
        },
        error: function(error){
            console.assert(error.responseText);
        }
        });

}

function callApiGetLatestProductData(){
    
    $.ajax({
        url: 'http://localhost:8080/products/latest' ,
        type: "GET",
        dataType: 'json',
        async:false,
        success: function(responseObject){
        
            getDataLatestProduct(responseObject);
       
            loadProductFilterToShopHTML(gProductLatest);
        },
        error: function(error){
            console.assert(error.responseText);
        }
        });

}



var gProductLatest = {}
function getDataLatestProduct(responseObject){ 
    
    
    gProductLatest = Array.from({ length: responseObject.length } , () => ({ id: '',
                                                                         productCode: '',
                                                                         productName:'', 
                                                                        productDescription:'',
                                                                            productScale:'',
                                                                         productVendor:'',
                                                                        quantityInStock:'',
                                                                     buyPrice:'',
                                                                     imgURL:'',}))
    
      
    
            for (var i=0; i<gProductLatest.length;i++){ 
                gProductLatest[i].id = responseObject[i][0];
                gProductLatest[i].buyPrice = responseObject[i][1];
                gProductLatest[i].productCode = responseObject[i][2];
                gProductLatest[i]. productDescription = responseObject[i][3];
                gProductLatest[i].productName = responseObject[i][4];
                gProductLatest[i].productScale = responseObject[i][5];

                gProductLatest[i].productVendor = responseObject[i][6];
                gProductLatest[i].quantityInStock = responseObject[i][7];
                gProductLatest[i].imgURL = responseObject[i][9];
                
            } 
            
            return gProductLatest;
        
    
}

function getDataHotProduct(responseObject){ 
    gProductHot = Array.from({ length: responseObject.length } , () => ({ id: '',
                                                                         productCode: '',
                                                                         productName:'', 
                                                                        productDescription:'',
                                                                            productScale:'',
                                                                         productVendor:'',
                                                                        quantityInStock:'',
                                                                     buyPrice:'',
                                                                     imgURL:'',}))
            for (var i=0; i<gProductHot.length;i++){
                gProductHot[i].id = responseObject[i][0];
                gProductHot[i].buyPrice = responseObject[i][1];
                gProductHot[i].productCode = responseObject[i][2];
                gProductHot[i]. productDescription = responseObject[i][3];
                gProductHot[i].productName = responseObject[i][4];
                gProductHot[i].productScale = responseObject[i][5];

                 gProductHot[i].productVendor = responseObject[i][6];
                 gProductHot[i].quantityInStock = responseObject[i][7];
                 gProductHot[i].imgURL = responseObject[i][9];
                
            } 
            return gProductHot;
        }

        var gProductLatest = {}
function getDataPriceFilterProduct(responseObject){ 
    gProductPriceFilter = Array.from({ length: responseObject.length } , () => ({ id: '',
                                                                            productCode: '',
                                                                            productName:'', 
                                                                        productDescription:'',
                                                                            productScale:'',
                                                                            productVendor:'',
                                                                        quantityInStock:'',
                                                                        buyPrice:'',
                                                                        imgURL:'',}))
            for (var i=0; i<gProductPriceFilter.length;i++){
                gProductPriceFilter[i].id = responseObject[i][0];
                gProductPriceFilter[i].buyPrice = responseObject[i][1];
                gProductPriceFilter[i].productCode = responseObject[i][2];
                gProductPriceFilter[i]. productDescription = responseObject[i][3];
                gProductPriceFilter[i].productName = responseObject[i][4];
                gProductPriceFilter[i].productScale = responseObject[i][5];

                gProductPriceFilter[i].productVendor = responseObject[i][6];
                gProductPriceFilter[i].quantityInStock = responseObject[i][7];
                gProductPriceFilter[i].imgURL = responseObject[i][9];
                
            } 
            return gProductPriceFilter;
        }



       


     

// init Isotope
var $grid = $('.area-filter')
// filter items on button click

// jQuery
// hash of functions that match data-filter values
var filterFns = { 
    // show if number is greater than 50
    numberLessThan500: function() {
        // use $(this) to get item element
        var number = $(this).find('.filterPrice').data('price');
        return parseInt( number, 10 ) < 500;
    },
    numberOver500Less1000: function() { 
        // use $(this) to get item element
        var number = $(this).find('.filterPrice').data('price');
        if(parseInt(number)>500 && parseInt(number)<1000){

            return parseInt(number, 10  );
        }
    },
    numberOver1000Less3000: function() { 
        // use $(this) to get item element
        var number = $(this).find('.filterPrice').data('price');
        if(parseInt(number)>1000 && parseInt(number)<3000){

            return parseInt(number, 10  );
        }
    },
    numberOver3000: function() { 
        // use $(this) to get item element
        var number = $(this).find('.filterPrice').data('price');
        if(parseInt(number)>3000 ){

            return parseInt(number, 10  );
        }
    },
   
    
};
$('.price-wrap ').change(function() {   
    "use strict"
   var vValue= $('.price-wrap input[type=radio]:checked').val()
   $.ajax({
    url: 'http://localhost:8080/products/' + vValue ,
    type: "GET",
    dataType: 'json',
    async:false,
    success: function(responseObject){ 
        
        getDataPriceFilterProduct(responseObject);
       console.log(gProductPriceFilter)
        loadProductFilterToShopHTML(gProductPriceFilter);
    },
    error: function(error){
        console.assert(error.responseText);
    }
   })
   
   
    
});

//Pagination
function loadPagination(){ 

    var items = $(".product-area .item");
    var numItems = items.length;
    var perPage = 4;
    items.slice(perPage).hide();
    
    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
}

function removeActiveClassBtn(){ 
    $('.btn-category').removeClass('active')
}






})