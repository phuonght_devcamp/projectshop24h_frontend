jQuery(document).ready(function($){ 
 
    checkCookieExist();
    function checkCookieExist() {  
        var myCookie = getCookie("token");
    
        if (myCookie !== "") {
            // do cookie  exist
            $("#nav_login").remove();
            $("#menu_link").append('<li id="nav_login"><a href=""><i class="fa fa-user"></i> <span id="logout">Logout</span></a></li>');
        }
       return false;
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function deleteCookie(name){
        document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
    
    $("#logout").on('click',function(){
        deleteCookie("token");
        window.location.href="index.html"
        sessionStorage.clear();
    })
    // jQuery sticky Menu
    callApiGetProductData();
	$(".mainmenu-area").sticky({topSpacing:0});
    
    
    $('.product-carousel').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:5,
            }
        }
    });  
    
    $('.related-products-carousel').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:2,
            },
            1000:{
                items:2,
            },
            1200:{
                items:3,
            }
        }
    });  
    
    $('.brand-list').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });    
    
    
    // Bootstrap Mobile Menu fix
    $(".navbar-nav li a").click(function(){
        $(".navbar-collapse").removeClass('in');
    });    
    
    // jQuery Scroll effect
    $('.navbar-nav li a, .scroll-to-up').bind('click', function(event) {
        var $anchor = $(this);
        var headerH = $('.header-area').outerHeight();
        $('html, body').stop().animate({
            scrollTop : $($anchor.attr('href')).offset().top - headerH + "px"
        }, 1200, 'easeInOutExpo');

        event.preventDefault();
    });    


    // --------------------------------------------------------------------------------------------------
    var gProductDB={};


    // Get data product
   

    function callApiGetProductData(){
        "use strict";
        $.ajax({
        url: 'http://localhost:8080/products' ,
        type: "GET",
        dataType: 'json',
        async:false,
        success: function(responseObject){
        
        gProductDB=responseObject;
        loadProductToIndexHTML(gProductDB);
        loadProductToShopHTML(gProductDB);
        },
        error: function(error){
            console.assert(error.responseText);
        }
        });
    }

    function loadProductToIndexHTML(){
        console.log(gProductDB)
        var vProductSection=``;
        for(var bI =0; bI < gProductDB.length; bI++ ){
        var vProductSection = `
       
        <div class="single-product">
            <div class="product-f-image">
                <img src="`+gProductDB[bI].imgURL+`" alt="">
                <div class="product-hover">
                    <a href="#" class="add-to-cart-link" data-id="`+gProductDB[bI].id+`" data-img="`+gProductDB[bI].imgURL+`" data-name="`+gProductDB[bI].productName+`" data-price="`+gProductDB[bI].buyPrice+`"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    <a href="#" class="view-details-link" data-id="`+gProductDB[bI].id+`"><i class="fa fa-link"></i> See details</a>
                </div>
            </div>
            
            <h2><a href="single-product.html">`+gProductDB[bI].productName+`</a></h2>
            
            <div class="product-carousel-price">
                <ins>$`+gProductDB[bI].buyPrice+`</ins> 
            </div> 
        </div>
        
         `;
         $(".product-carousel ").append(vProductSection)
        }
    }

    function loadProductToShopHTML(){
        //console.log(gProductDB)
        var vProductSection=``;
        for(var bI =0; bI < gProductDB.length; bI++ ){
        var vProductSection = `
       
        <div class="col-md-3 col-sm-6 mt-3">
            <div class="single-shop-product"  >
                <div class="product-upper" >
                    <img src="`+gProductDB[bI].imgURL+`" alt="" width="70%">
                </div>
                <h6><a >`+gProductDB[bI].productName+`</a></h6>
                <div class="product-carousel-price">
                    <ins>$`+gProductDB[bI].buyPrice+`</ins> 
                </div>  
                
                <div class="product-option-shop">
                    <a class="add_to_cart_button" data-id="`+gProductDB[bI].id+`" data-img="`+gProductDB[bI].imgURL+`" data-name="`+gProductDB[bI].productName+`" data-price="`+gProductDB[bI].buyPrice+`">Add to cart</a>
                </div>                       
            </div>
        </div>
        
         `
         ;
         $(".product-area ").append(vProductSection)
        }
    }

    var gProductObj = {};

    $(".view-details-link").on("click", function(){
        
      var vId = $(this).data('id');
      sessionStorage.setItem("productId", vId );
         //console.log(gProductObj)
         window.location.href="single-product.html"
    })

    


});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10146041-21', 'auto');
  ga('send', 'pageview');
