jQuery(document).ready(function ($) {
  // ************************************************
  // Shopping Cart API
  // ************************************************
  checkCookieExist();
  function checkCookieExist() {
    var myCookie = getCookie("token");

    if (myCookie !== "") {
      // do cookie doesn't exist stuff;
      $("#nav_login").remove();
      $("#menu_link").append(
        '<li id="nav_login"><i class="fa fa-user"></i> <span id="logout">Logout</span></li>'
      );
    }
    return false;
  }
  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  function deleteCookie(name) {
    document.cookie =
      name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }

  $("#logout").on("click", function () {
    deleteCookie("token");
    window.location.href = "index.html";
    sessionStorage.clear();
  });
  var shoppingCart = (function () {
    // =============================
    // Private methods and propeties
    // =============================
    cart = [];

    // Constructor
    function Item(id, name, price, count, imgURL, total) {
      this.id = id;
      this.name = name;
      this.price = price;
      this.count = count;
      this.imgURL = imgURL;
      this.total = price * count;
    }

    // Save cart
    function saveCart() {
      sessionStorage.setItem("shoppingCart", JSON.stringify(cart));
    }

    // Load cart
    function loadCart() {
      cart = JSON.parse(sessionStorage.getItem("shoppingCart"));
    }
    if (sessionStorage.getItem("shoppingCart") != null) {
      loadCart();
    }

    // =============================
    // Public methods and propeties
    // =============================
    var obj = {};

    // Add to cart
    obj.addItemToCart = function (id, name, price, count, imgURL) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart[item].count++;
          saveCart();
          return;
        }
      }
      var item = new Item(id, name, price, count, imgURL);
      cart.push(item);
      saveCart();
    };
    // Add many to cart
    obj.addManyItemToCart = function (name, price, count, imgURL, total) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart[item].count = cart[item].count + count;
          saveCart();
          return;
        }
      }
      var item = new Item(name, price, count, imgURL, total);
      cart.push(item);
      saveCart();
    };
    // Set count from item
    obj.setCountForItem = function (name, count) {
      for (var i in cart) {
        if (cart[i].name === name) {
          cart[i].count = count;
          cart[i].total = cart[i].count * cart[i].price;
          break;
        }
      }
      saveCart();
    };
    // find index item in cart
    obj.findIndexItem = function (name) {
      for (var i in cart) {
        if (cart[i].name === name) {
          return i;
        }
      }
    };
    // Remove item from cart
    obj.removeItemFromCart = function (name) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart[item].count--;
          if (cart[item].count === 0) {
            cart.splice(item, 1);
          }
          break;
        }
      }
      saveCart();
    };

    // Remove all items from cart
    obj.removeItemFromCartAll = function (name) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart.splice(item, 1);
          break;
        }
      }
      saveCart();
    };

    // Clear cart
    obj.clearCart = function () {
      cart = [];
      saveCart();
    };

    // Count cart
    obj.totalCount = function () {
      var totalCount = 0;
      for (var item in cart) {
        totalCount += cart[item].count;
      }
      return totalCount;
    };

    // Total cart
    obj.totalCart = function () {
      var totalCart = 0;
      for (var item in cart) {
        totalCart += cart[item].price * cart[item].count;
      }
      return Number(totalCart.toFixed(2));
    };

    // update cart
    obj.updateCart = function () {
      var totalItem = 0;
      for (var item in cart) {
        totalItem = cart[item].price * cart[item].count;
      }
    };

    // List cart
    obj.listCart = function () {
      var cartCopy = [];
      for (i in cart) {
        item = cart[i];
        itemCopy = {};
        for (p in item) {
          itemCopy[p] = item[p];
        }
        itemCopy.total = Number(item.price * item.count).toFixed(2);
        cartCopy.push(itemCopy);
      }
      return cartCopy;
    };

    // cart : Array
    // Item : Object/Class
    // addItemToCart : Function
    // removeItemFromCart : Function
    // removeItemFromCartAll : Function
    // clearCart : Function
    // countCart : Function
    // totalCart : Function
    // listCart : Function
    // saveCart : Function
    // loadCart : Function
    return obj;
  })();

  // *****************************************
  // Triggers / Events
  // *****************************************
  // Add item
  $(".add-to-cart-link").click(function (event) {
    event.preventDefault();
    var id = $(this).data("id");
    var name = $(this).data("name");
    var price = Number($(this).data("price"));
    var imgURL = $(this).data("img");
    shoppingCart.addItemToCart(id, name, price, 1, imgURL);
    displayCart();
  });

  $(".single-product-area").on(
    "click",
    ".add_to_cart_button",
    function (event) {
      event.preventDefault();
      var id = $(this).data("id");
      var name = $(this).data("name");
      var price = Number($(this).data("price"));
      var imgURL = $(this).data("img");
      shoppingCart.addItemToCart(id, name, price, 1, imgURL);
      displayCart();
    }
  );

  $(".add_to_cart_button_multiple").click(function (event) {
    event.preventDefault();
    $(this).data("quantity", $("#prod-qty").val());

    var id = $(this).data("id");
    var name = $(this).data("name");
    var price = Number($(this).data("price"));
    var count = Number($(this).data("quantity"));
    var imgURL = $(this).data("img");
    console.log(price);
    shoppingCart.addManyItemToCart(id, name, price, count, imgURL);
    displayCart();
  });

  // Clear items
  $(".clear-cart").click(function () {
    shoppingCart.clearCart();
    displayCart();
  });

  function displayCart() {
    var cartArray = shoppingCart.listCart();
    var output = "";
    for (var i in cartArray) {
      output +=
        `<tr class="cart_item">` +
        `<td class="product-remove"><i type="submit" style="color:red;" class="fa fa-minus-circle" id="remove-item" data-name="` +
        cartArray[i].name +
        `"></i></td>` +
        `<td class="product-thumbnail">
                <a href="single-product.html"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="` +
        cartArray[i].imgURL +
        `"></a>
            </td>
            <td class="product-name">
                <a href="single-product.html">` +
        cartArray[i].name +
        `</a> 
            </td>` +
        `<td class="product-price">
                <span class="amount">` +
        cartArray[i].price +
        `$</span> 
            </td>` +
        `<td class="product-quantity ">
                <div class="number1">
                    <span class="minus1" data-name="` +
        cartArray[i].name +
        `" >-</span>
                        <input id="input1" type="text"  value="` +
        cartArray[i].count +
        `"/>
                    <span class="plus1" data-name="` +
        cartArray[i].name +
        `">+</span>
                </div>

            </td>` +
        `<td class="product-subtotal">
                <span class="amount">` +
        cartArray[i].count * cartArray[i].price +
        `$</span> 
            </td>` +
        `</tr>`;
    }
    $("#cart-table tbody").append(output);
    $(".cart-amunt").html(shoppingCart.totalCart());
    $(".product-count").html(shoppingCart.totalCount());
  }

  // Delete item button

  $(".shop_table tbody").on("click", "#remove-item", function () {
    var name = $(this).data("name");
    shoppingCart.removeItemFromCartAll(name);
    $(".cart-amunt").html(shoppingCart.totalCart());
    $(".product-count").html(shoppingCart.totalCount());
    $(this).closest("tr").remove();
  });

  // -1
  $(".shop_table tbody").on("click", ".minus1", function () {
    var $input = $(this).parent().find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    var name = $(this).data("name");

    var count = parseInt($input.val());
    shoppingCart.setCountForItem(name, count);
    var i = shoppingCart.findIndexItem(name);
    $(this)
      .closest("td")
      .next("td")
      .find("span")
      .html("$" + cart[i].total);
    // displayCart();
    $(".cart-amunt").html(shoppingCart.totalCart());
    $(".product-count").html(shoppingCart.totalCount());
    return false;
  });
  // +1
  $(".shop_table tbody").on("click", ".plus1", function () {
    var name = $(this).data("name");

    var $input = $(this).parent().find("input");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    var count = parseInt($input.val());
    shoppingCart.setCountForItem(name, count);
    var i = shoppingCart.findIndexItem(name);
    $(this)
      .closest("td")
      .next("td")
      .find("span")
      .html("$" + cart[i].total);

    // $('.cart-amunt').html(shoppingCart.totalCart());
    $(".cart-amunt").html(shoppingCart.totalCart());
    $(".product-count").html(shoppingCart.totalCount());
    // displayCart();
    return false;
  });

  displayCart();
  loadOrderCheckout();

  // subtotal
  $(".total_amount").text(shoppingCart.totalCart).append("$");
  $(".total_order").text(shoppingCart.totalCart).append("$");

  // load item to check out
  function loadOrderCheckout() {
    var cartArray = shoppingCart.listCart();
    var output = "";
    var totalCart = 0;
    for (var i in cartArray) {
      output +=
        `<tr class="cart_item">` +
        `
            <td class="product-name">
                <a>` +
        cartArray[i].name +
        ` x` +
        cartArray[i].count +
        `</a> 
            </td>` +
        `<td class="product-price">
                <span class="amount">` +
        cartArray[i].total +
        `$</span> 
            </td>` +
        `</tr>`;
      totalCart += cartArray[i].price * cartArray[i].count;
    }

    $("#checkout-table tbody").append(output);
    console.log(typeof shoppingCart.totalCart);
    $(".cart-subtotal span").append(totalCart.toFixed(2)).append("$");
    $(".order-total span").append(totalCart.toFixed(2)).append("$");
  }
});
