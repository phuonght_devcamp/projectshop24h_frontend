
    $(document).ready(function () {
        // Lưu một biến token để dùng khi gọi chức năng get user info 
        var gToken = "";

        const gBASE_URL = "http://localhost:8080"

        $("#btn-signup").on("click", function() { 
            getUserData();
            signUpFunction();
        });

        // $("#signin").on("click", function() {
        //     signInFunction();
        // });

        // $("#info").on("click", function() {
        //     infoFunction();
        // });

        var gSignUpObj = {
            
            username: '',
            email: '',
            password: '',
            reTypePassword:''
        }

        function getUserData(){ 
            gSignUpObj.username=$('#inputUsername').val().trim();
            gSignUpObj.email=$('#inputEmail').val().trim();
            gSignUpObj.password=$('#inputPassword').val().trim();
            gSignUpObj.reTypePassword=$('#inputPassword-confirm').val().trim();
           
        }
         // Hàm validate email bằng regex
        function validateEmail(email) { 
            const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return regex.test(String(email).toLowerCase());
        }
        //Validate dữ liệu từ form
        function validateForm(gSignUpObj) {
            if(!validateEmail(gSignUpObj.email)) {
                alert("Email không phù hợp");
                return false;
            };

            if(gSignUpObj.password === "") {
                alert("Password không phù hợp");
                return false;
            }
           
            if(gSignUpObj.reTypePassword !==  gSignUpObj.password) {
                alert("Password nhập vào không trùng nhau");
                return false;
            }
            return true;
        }

        function signUpFunction() {  
           
           var vSignUpRequest = {}
           vSignUpRequest.username = gSignUpObj.username
           vSignUpRequest.email = gSignUpObj.email
           vSignUpRequest.password = gSignUpObj.password
           var vIsdataValid = validateForm(gSignUpObj);
           if(vIsdataValid )
            {   
               
                $.ajax({
                    url: gBASE_URL + "/register",
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(vSignUpRequest),
                    success: function (pRes) {
                        console.log(pRes)
                        alert("Sign up success!")
                        window.location.href = "login.html"
                    },
                    error: function (pAjaxContext) {
                        console.log(pAjaxContext.responseJSON.message);
                    }
                });
            }
      }
        // function signInFunction() {
        //     var vLoginData = {
        //         email: "minhvd@devcamp.edu.vn",
        //         password: "12345678"
        //     }

        //     $.ajax({
        //         url: "http://42.115.221.44:8080/devcamp-auth/users/signin?email=" + vLoginData.email + '&password=' + vLoginData.password,
        //         type: 'POST',
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         success: function (pRes) {
        //             console.log(pRes);

        //             gToken = pRes.token;
        //         },
        //         error: function (pAjaxContext) {
        //             console.log(pAjaxContext.responseText);
        //         }
        //     });
        // }

        // function infoFunction() {
        //     if(gToken == "") {
        //         alert("Chưa có token");
        //         return;
        //     }

        //     $.ajax({
        //         url: "http://42.115.221.44:8080/devcamp-auth/users/me",
        //         type: 'GET',
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         headers: {
        //             "Authorization": "Bearer " + gToken
        //         },
        //         success: function (pRes) {
        //             console.log(pRes);
        //         },
        //         error: function (pAjaxContext) {
        //             console.log(pAjaxContext.responseText);
        //         }
        //     });
        // }
    })
